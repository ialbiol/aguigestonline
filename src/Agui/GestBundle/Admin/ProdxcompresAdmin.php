<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ProdxcompresAdmin extends Admin {
 protected $baseRouteName = 'prodxcompres';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   //copiar de la compra i del producte
   //->add('idllibre', 'text', array('label' => 'nom', 'required' => true))
   //->add('idtipo', 'text', array('label' => 'nom', 'required' => true))
   ->add('producte','sonata_type_model_list', array('label' => 'Producte','required' => false), array())
   ->add('nom', 'text', array('label' => 'Producte extern', 'required' => false))  
   ->add('quantitat', 'text', array('label' => 'Quantitat', 'required' => true))
   ->add('cost', 'text', array('label' => 'Preu', 'required' => true))
   ->add('dte', 'text', array('label' => 'Descompte', 'required' => true))
   ->add('total', 'text', array('label' => 'Subtotal', 'required' => false, 'read_only' => true))
  ;        
 }

}