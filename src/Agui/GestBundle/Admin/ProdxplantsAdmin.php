<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ProdxplantsAdmin extends Admin {
 protected $baseRouteName = 'prodxplants';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
			->add('producte','sonata_type_model_list', array('label' => 'Producte','required' => true), array())
   ->add('quantitat', 'text', array('label' => 'Quantitat', 'required' => true))
			->add('percent', 'number', array('label' => 'Percentatge', 'required' => false, 'read_only' => true))
   ->add('qpre', 'text', array('label' => 'Total', 'required' => false, 'read_only' => true))
  ;        
 }

}