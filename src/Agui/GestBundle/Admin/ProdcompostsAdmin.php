<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ProdcompostsAdmin extends Admin {
 protected $baseRouteName = 'prodcomposts';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('productepart','sonata_type_model_list', array('label' => 'Producte','required' => true), array())
			->add('preualbaran', 'number', array('label' => 'Preu albaran', 'required' => false, 'read_only' => true))
   ->add('quantitat', 'text', array('label' => 'quantitat', 'required' => true))
			->add('subtotal', 'number', array('label' => 'Subtotal', 'required' => false, 'read_only' => true))
  ;        
 }

}