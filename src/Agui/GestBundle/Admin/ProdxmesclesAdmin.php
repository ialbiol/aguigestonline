<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ProdxmesclesAdmin extends Admin {
 protected $baseRouteName = 'prodxmescles';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('producte','sonata_type_model_list', array('label' => 'Producte','required' => true), array())
			->add('preualbaran', 'number', array('label' => 'Preu albaran', 'required' => false, 'read_only' => true))
			->add('num', 'number', array('label' => 'Numero', 'required' => true))
   ->add('percent', 'number', array('label' => 'Percentatge', 'required' => false, 'read_only' => true))
			->add('preupercent', 'number', array('label' => 'Preu %', 'required' => false, 'read_only' => true))
  ;        
 }

}