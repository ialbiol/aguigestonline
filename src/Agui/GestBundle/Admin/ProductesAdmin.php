<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class ProductesAdmin extends Admin {
	protected $baseRoutePattern = 'productes';
 protected $baseRouteName = 'productes';

 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
		;

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Producte'))
			->add('idfiltrotext', null, array('label' => 'Normal / albaran'))
			->add('tipoprod.nom', null, array('label' => 'Tipo producte'))
   ->add('pcompra', null, array('label' => 'Preu compra / fabricació'))
   ->add('pfactura', null, array('label' => 'Preu venta'))
			->add('beneficiperunitat', null, array('label' => 'Benefici per unitat'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Producte'))
   ->add('idfiltro', 'doctrine_orm_string', array('label' => 'Normal / albaran'), 'choice', array('choices' => Auxselects::GetFiltroProducte()))
   ->add('tipoprod', null, array('label' => 'Tipo producte'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->tab('Producte')->with('')
    ->add('tipoprod', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Tipoprods',
     'property' => 'nom',
     'label' => 'Tipo producte',
     'required' => true
    ))
    ->add('nom', 'text', array('label' => 'Nom', 'required' => true))
    ->add('marca', 'text', array('label' => 'Marca', 'required' => false))
    ->add('refcolor', 'text', array('label' => 'Referencia color', 'required' => false))
    ->add('acabats', 'text', array('label' => 'Acabat', 'required' => false))
    //->add('idllibre', 'text', array('label' => 'idllibre', 'required' => true))
    ->add('qalmacen', 'number', array('label' => 'Quantitat almacen', 'required' => false))
    ->add('qminima', 'number', array('label' => 'Quantitat mínima', 'required' => false))
    ->add('pcompra', 'number', array('label' => 'Preu compra / fabricació', 'required' => false, 'read_only' => true))
    ->add('pfactura', 'number', array('label' => 'Preu venta', 'required' => false))
    ->add('percent', 'number', array('label' => 'Percentatge', 'required' => false))
    ->add('notes', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
    ->add('idfiltro', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetFiltroProducte(),
					'required' => true,
					'label' => 'Normal / albaran',
					'attr' => array('style' => 'width:100%;')))
   ->end()->end()

   ->tab('Dades compra')->with('')
    ->add('palbaran', 'number', array('label' => 'Preu albaran', 'required' => false))
    ->add('unitats', 'number', array('label' => 'unitats', 'required' => false))
   ->end()->end()

   ->tab('Mescla')->with('')
    ->add('prodbase', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Mescla'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.prodxmescles'))
				->add('preumescla', 'number', array('label' => 'Preu compra mescla', 'required' => false, 'read_only' => true))
   ->end()->end()

   ->tab('Composició')->with('')
    ->add('prodcompost', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Composició'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.prodcomposts'))
				->add('preucomposicio', 'number', array('label' => 'Preu composicio', 'required' => false, 'read_only' => true))
   ->end()->end()

//   ->tab('Total Factures')->with('')

//   ->end()->end()

//   ->tab('Total compres')->with('')

//   ->end()->end()

   ->tab('Imatges')->with('')
				->add('fotoproducte', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Imatges'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.fotos'))
   ->end()->end()

   ->tab('Notes')->with('')
    ->add('notesp', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '24', 'style' => 'width:100%;')))
   ->end()->end()
  ;        
 }

	public function prePersist($object){
		$this->SaveObject($object);
	}

	public function preUpdate($object){
		$this->SaveObject($object);
	}

	protected function SaveObject($object){
		$this->saveFile($object);

		$object->setPcompra($object->getPalbaran() / $object->getUnitats());

		//mescles
		$data = $object->getProdbase();
		$totNum = 0;
		foreach($data as $dat){
			$dat->setBase($object);
			$totNum = $totNum + $dat->getNum();
		}
		foreach($data as $dat){
			if($totNum > 0){ $per = ($dat->getNum() / $totNum) * 100; }
			else { $per = 0; }
			$dat->setPercent($per);
		}

		//composicions
		$data = $object->getProdcompost();
		foreach($data as $dat){
			$dat->setProducte($object);
		}
	}

	public function saveFile($object) {
		$em = $this->getModelManager();
		$attmanager = $this->getConfigurationPool()->getContainer()->get('Attachmentmanager');

		foreach ($object->getFotoproducte() as $attach) {
			$attach->setProducte($object);

			if($attach->getId() == null) {
				$em->create($attach);
			}

			$attmanager->upload($attach);
		}
	}

}