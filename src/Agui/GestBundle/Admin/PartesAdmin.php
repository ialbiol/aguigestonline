<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class PartesAdmin extends Admin {
	protected $baseRoutePattern = 'partes';
 protected $baseRouteName = 'partes';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
			->andWhere("o.data >= '" . $maininfo->anyActualInici . "' AND o.data <= '" . $maininfo->anyActualFinal ."'");

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
			->addIdentifier('treballador.nom', null, array('label' => 'Treballador'))
			->addIdentifier('data', null, array('label' => 'Data'))
			->addIdentifier('hinici', null, array('label' => 'Hora inici'))
			->addIdentifier('hfinal', null, array('label' => 'Hora final'))
			->addIdentifier('factura.refclient', null, array('label' => 'Treball'))
   ->addIdentifier('descripcio', null, array('label' => 'Descripció'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
			->add('data', null, array('label' => 'Data'))
			->add('treballador', null, array('label' => 'Treballador'))
			->add('factura.refclient', null, array('label' => 'Treball'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
			->add('empresa', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Empreses',
     'property' => 'nom',
     'label' => 'Empresa',
     'required' => true
    ))
			->add('treballador', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Treballadors',
     'property' => 'nom',
     'label' => 'Treballador',
     'required' => true
    ))
/*			->add('factura', 'entity', array(
     'attr' => array('class' => 'select2', 'admin_code' => 'sonata.admin.treballs'),
     'class' => 'Agui\GestBundle\Entity\Factures',
     'property' => 'refclient',
     'label' => 'Treball',
					'admin_code' => 'sonata.admin.treballs',
     'required' => true
    ), array('sonata_admin' => 'sonata.admin.treballs'))//*/
/*			->add('data', 'datetime', array('label' => 'Data', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => true, 'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
   ->add('hinici', 'datetime', array('label' => 'Hora inici', 'required' => true))
   ->add('hfinal', 'datetime', array('label' => 'Hora final', 'required' => false))*/
   //->add('opcions', 'text', array('label' => 'opcions', 'required' => true))
   ->add('descripcio', 'text', array('label' => 'descripcio', 'required' => false))
			->add('notes', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
  ;        
 }

}