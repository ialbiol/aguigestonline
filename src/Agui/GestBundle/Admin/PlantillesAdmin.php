<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;
use Doctrine\ORM\EntityRepository;

class PlantillesAdmin extends Admin {
	protected $baseRoutePattern = 'plantilles';
 protected $baseRouteName = 'plantilles';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
		;

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Nom plantilla'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
			->add('idtoca', null, array('label' => 'Models'), 'choice', array('choices' => Auxselects::GetPlantillesModels()))
			->add('tipoun', null, array('label' => 'Tipo'), 'choice', array('choices' => Auxselects::GetPlantillesTipo()))
   ->add('nom', null, array('label' => 'Nom plantilla'))

			->add('idtpclient', 'doctrine_orm_callback', array('label' => 'Client',
				'callback' => function($queryBuilder, $alias, $field, $value) {
					// print_r($value); die;
					if (!$value["value"]) {
						return;
					}
					$queryBuilder->andWhere('u.tipo = :tipo')
						->setParameter('tipo', 0)
					;
					return true;
				}
			))
  ;
 }

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
		global $idtoca;
		$idtoca = $this->getSubject()->getIdtoca();
		if($idtoca == null){ $idtoca = 0; } //normal per defecte

  $formMapper
			->tab('Dades')->with('')
				//tipo de client
				->add('idtpclient', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipoplans', 'query_builder' => function(EntityRepository $er) {
						global $idtoca;
						return $er->createQueryBuilder('u')
								->where('u.tipo = :tipo')
								->andWhere('u.idtoca = :idtoca')
								->orderby('u.nom')
								->setParameter('tipo', 0)
								->setParameter('idtoca', $idtoca);
					},
					'property' => 'nom', 'required' => true, 'attr' => array('style' => 'width: 100%'),'label' => 'Client'))

				//tipo de treball
				->add('idtptreb', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipoplans', 'query_builder' => function(EntityRepository $er) {
						global $idtoca;
						return $er->createQueryBuilder('u')
								->where('u.tipo = :tipo')
								->andWhere('u.idtoca = :idtoca')
								->orderby('u.nom')
								->setParameter('tipo', 1)
								->setParameter('idtoca', $idtoca);
					},
					'property' => 'nom', 'required' => true, 'attr' => array('style' => 'width: 100%'),'label' => 'Tipo faena'))

				//clase de treball
				->add('idtpttreb', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipoplans', 'query_builder' => function(EntityRepository $er) {
						global $idtoca;
						return $er->createQueryBuilder('u')
								->where('u.tipo = :tipo')
								->andWhere('u.idtoca = :idtoca')
								->orderby('u.nom')
								->setParameter('tipo', 2)
								->setParameter('idtoca', $idtoca);
					},
					'property' => 'nom', 'required' => true, 'attr' => array('style' => 'width: 100%'),'label' => 'Clase faena'))

				//aplicacio sobre
				->add('idtipoplan', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipoplans', 'query_builder' => function(EntityRepository $er) {
						global $idtoca;
						return $er->createQueryBuilder('u')
								->where('u.tipo = :tipo')
								->andWhere('u.idtoca = :idtoca')
								->orderby('u.nom')
								->setParameter('tipo', 3)
								->setParameter('idtoca', $idtoca);
					},
					'property' => 'nom', 'required' => true, 'attr' => array('style' => 'width: 100%'),'label' => 'Aplicació sobre'))

				//tipo acabat
				->add('idtpacabat', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipoplans', 'query_builder' => function(EntityRepository $er) {
						global $idtoca;
						return $er->createQueryBuilder('u')
								->where('u.tipo = :tipo')
								->andWhere('u.idtoca = :idtoca')
								->orderby('u.nom')
								->setParameter('tipo', 4)
								->setParameter('idtoca', $idtoca);
					},
					'property' => 'nom', 'required' => true, 'attr' => array('style' => 'width: 100%'),'label' => 'Tipo acabat'))

				//clase acabat
				->add('idtpcacabat', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipoplans', 'query_builder' => function(EntityRepository $er) {
						global $idtoca;
						return $er->createQueryBuilder('u')
								->where('u.tipo = :tipo')
								->andWhere('u.idtoca = :idtoca')
								->orderby('u.nom')
								->setParameter('tipo', 5)
								->setParameter('idtoca', $idtoca);
					},
					'property' => 'nom', 'required' => true, 'attr' => array('style' => 'width: 100%'),'label' => 'Clase acabat'))

				->add('nom', 'text', array('label' => 'Nom', 'required' => true))
				->add('notes', 'textarea', array('label' => 'Descripcio', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))

				//tipo plantilla: normal o per unitat
				->add('tipoun', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetPlantillesTipo(),
					'required' => true,
					'label' => 'Tipo',
					'attr' => array('style' => 'width:100%;')))
	
				//tipo plantilla: normal, model, especial, fora
				->add('idtoca', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetPlantillesModels(),
					'required' => true,
					'label' => 'Models',
					'attr' => array('style' => 'width:100%;')))

				//->add('rutaimg1', 'text', array('label' => 'rutaimg1', 'required' => false))
				//->add('rutaimg2', 'text', array('label' => 'rutaimg2', 'required' => false))
			->end()->end()

			->tab('Productes')->with('')
				->add('prodxplant', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Productes'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.prodxplants'))
			->end()->end()

			->tab('Enviada a clients')->with('')
				->add('plantxclient', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Clients'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.plantxclients'))
			->end()->end()

   ->tab('Imatges')->with('')
				->add('fotoplantilla', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Imatges'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.fotos'))
   ->end()->end()

			->tab('Notes')->with('')
				->add('notes2', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '24', 'style' => 'width:100%;')))
			->end()->end()
  ;        
 }

	public function prePersist($object){
		$this->SaveObject($object);
	}

	public function preUpdate($object){
		$this->SaveObject($object);
	}

	protected function SaveObject($object){
		$this->saveFile($object);

		//productes
		$data = $object->getProdxplant();
		foreach($data as $dat){
			$dat->setPlantilla($object);
			$qpre = $dat->getNQuantitat() + (($dat->getNQuantitat() * $dat->getPercent()) / 100);
			$dat->setQpre($qpre);
		}

		//clients
		$data = $object->getPlantxclient();
		foreach($data as $dat){
			$dat->setPlantilla($object);
		}
	}

	public function saveFile($object) {
		$em = $this->getModelManager();
		$attmanager = $this->getConfigurationPool()->getContainer()->get('Attachmentmanager');

		foreach ($object->getFotoplantilla() as $attach) {
			$attach->setPlantilla($object);

			if($attach->getId() == null) {
				$em->create($attach);
			}

			$attmanager->upload($attach);
		}
	}

}