<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;

class FotosAdmin extends Admin {
 protected $baseRouteName = 'fotos';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Imatge'))
			->add('_action', 'actions', array(
				'actions' => array(
					'download' => array(
						'template' => 'AguiGestBundle:Button:download_button.html.twig'
					)
				)
			))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Imatge'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
		if ($this->getCurrentChild() == 0) {
			if ($this->getParentFieldDescription() != null) {
				if ($this->getParentFieldDescription()->getFieldName() == 'fotoproducte') {
					$formMapper
							->add('imagenamewpath', 'image', array(
								'required' => true,
								'label' => 'Imatge',
								'attr' => array('style' => 'width: 120px')
							))
							->add('attachmentid', 'downloadattachment', array(
								'required' => true,
								'label' => 'Attachment',
							))
						->add('tipofoto', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipofotos',	'query_builder' => function(EntityRepository $er) {
								return $er->createQueryBuilder('u')
										->where('u.grup = :grup')
										->orderby('u.orden')
										->setParameter('grup', 1);
							},
							'property' => 'nom', 'required' => true,	'attr' => array('style' => 'width: 200px'),	'label' => 'Tipo imatge'))
					;
				}

				if ($this->getParentFieldDescription()->getFieldName() == 'fotoplantilla') {
					$formMapper
							->add('imagenamewpath', 'image', array(
								'required' => true,
								'label' => 'Imatge',
								'attr' => array('style' => 'width: 120px')
							))
							->add('attachmentid', 'downloadattachment', array(
								'required' => true,
								'label' => 'Attachment',
							))
						->add('tipofoto', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipofotos',	'query_builder' => function(EntityRepository $er) {
								return $er->createQueryBuilder('u')
										->where('u.grup = :grup')
										->orderby('u.orden')
										->setParameter('grup', 10);
							},
							'property' => 'nom', 'required' => true,	'attr' => array('style' => 'width: 200px'),	'label' => 'Tipo imatge'))
					;
				}

				if ($this->getParentFieldDescription()->getFieldName() == 'fotofactura') {
					$formMapper
							->add('imagenamewpath', 'image', array(
								'required' => true,
								'label' => 'Imatge',
								'attr' => array('style' => 'width: 120px')
							))
							->add('attachmentid', 'downloadattachment', array(
								'required' => true,
								'label' => 'Attachment',
							))
						->add('tipofoto', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipofotos',	'query_builder' => function(EntityRepository $er) {
								return $er->createQueryBuilder('u')
										->where('u.grup = :grup')
										->orderby('u.orden')
										->setParameter('grup', 20);
							},
							'property' => 'nom', 'required' => true,	'attr' => array('style' => 'width: 200px'),	'label' => 'Tipo imatge'))
					;
				}

				if ($this->getParentFieldDescription()->getFieldName() == 'fototreballador') {
					$formMapper
							->add('imagenamewpath', 'image', array(
								'required' => true,
								'label' => 'Imatge',
								'attr' => array('style' => 'width: 120px')
							))
							->add('attachmentid', 'downloadattachment', array(
								'required' => true,
								'label' => 'Attachment',
							))
						->add('tipofoto', 'entity', array('class' => 'Agui\GestBundle\Entity\Tipofotos',	'query_builder' => function(EntityRepository $er) {
								return $er->createQueryBuilder('u')
										->where('u.grup = :grup')
										->orderby('u.orden')
										->setParameter('grup', 30);
							},
							'property' => 'nom', 'required' => true,	'attr' => array('style' => 'width: 200px'),	'label' => 'Tipo imatge'))
					;
				}

			}
		} else {
			$formMapper
				->add('nom', 'text', array('label' => 'nom', 'required' => false))
				->add('ruta', 'text', array('label' => 'ruta', 'required' => false))
			;
		}

		$formMapper
				->add('file', 'file', array('required' => false))
		;

 }

}