<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class ProveedorsAdmin extends Admin {
	protected $baseRoutePattern = 'proveedors';
 protected $baseRouteName = 'proveedors';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
		;

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Proveedor'))
   ->addIdentifier('malnom', null, array('label' => 'Mal nom'))
   ->addIdentifier('telf', null, array('label' => 'Telefon 1'))
   ->addIdentifier('telf2', null, array('label' => 'Telefon 2'))
   ->addIdentifier('mobil1', null, array('label' => 'Mobil 1'))
   ->addIdentifier('mobil2', null, array('label' => 'Mobil 2'))
   ->addIdentifier('email', null, array('label' => 'Email'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Proveedor'))
   ->add('malnom', null, array('label' => 'Mal nom'))
   ->add('telf', null, array('label' => 'Telefon 1'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->tab('Proveedor')->with('')
    ->add('nom', 'text', array('label' => 'Proveedor', 'required' => true))
    ->add('malnom', 'text', array('label' => 'Mal nom', 'required' => false))
    ->add('direccio', 'text', array('label' => 'Direccio', 'required' => false))
    ->add('email', 'text', array('label' => 'Email', 'required' => false))
    ->add('telf', 'text', array('label' => 'Telefono 1', 'required' => false))
    ->add('telf2', 'text', array('label' => 'Telefono 2', 'required' => false))
    ->add('telf3', 'text', array('label' => 'Telefono 3', 'required' => false))
    ->add('telf4', 'text', array('label' => 'Telefono 4', 'required' => false))
    ->add('mobil1', 'text', array('label' => 'Mobil 1', 'required' => false))
    ->add('mobil2', 'text', array('label' => 'Mobil 2', 'required' => false))
    ->add('mobil3', 'text', array('label' => 'Mobil 3', 'required' => false))
    ->add('mobil4', 'text', array('label' => 'Mobil 4', 'required' => false))
    ->add('nif', 'text', array('label' => 'Nif - Cif', 'required' => false))
    ->add('notes', 'text', array('label' => 'Notes', 'required' => false))
   ->end()->end()

   ->tab('Factures')->with('')
   ->end()->end()

   ->tab('Productes comprats')->with('')
   ->end()->end()

   ->tab('Albarans')->with('')
   ->end()->end()

   ->tab('Productes albarans')->with('')
   ->end()->end()

  ;
 }

}