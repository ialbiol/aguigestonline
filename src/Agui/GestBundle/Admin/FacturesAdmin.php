<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Sonata\AdminBundle\Route\RouteCollection;
use Agui\GestBundle\Entity\Auxselects;

class FacturesAdmin extends Admin {
 protected $baseRouteName = 'agui_gest_factures';
	protected $baseRoutePattern = 'factures';

 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'datafact'
	);

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("(o.estat < 30 AND o.estat >= 20)")
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
			->andWhere("o.datapres >= '" . $maininfo->anyActualInici . "' AND o.datapres <= '" . $maininfo->anyActualFinal ."'")
		;
		if($maininfo->clientActual != null){
			$query = $query
				->andWhere("(o.client = " . $maininfo->clientActual . " OR o.clientfact = " . $maininfo->clientActual .")")
			;
		}

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('id', null, array('label' => 'Codi', 'admin_code' => 'sonata.admin.factures'))
   ->addIdentifier('tnumfact', null, array('label' => 'Num', 'admin_code' => 'sonata.admin.factures'))
   ->addIdentifier('descripcio', null, array('label' => 'Descripció', 'admin_code' => 'sonata.admin.factures'))
   ->addIdentifier('refclient', null, array('label' => 'Ref Client', 'admin_code' => 'sonata.admin.factures'))
   ->addIdentifier('datafact', 'datetime', array('label' => 'Data Factura',	'format' => 'dd/MM/yyyy', 'admin_code' => 'sonata.admin.factures'))
   ->addIdentifier('totalfact', null, array('label' => 'Total Factura', 'admin_code' => 'sonata.admin.factures'))
   ->addIdentifier('estatfactura', null, array('label' => 'Estat', 'admin_code' => 'sonata.admin.factures'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('descripcio', null, array('label' => 'Descripció'))
   ->add('refclient', null, array('label' => 'Ref Client'))
   ->add('tnumfact', null, array('label' => 'Num'))
   ->add('estat', null, array('label' => 'Estat'), 'choice', array('choices' => Auxselects::GetEstatsFactures()))
   ->add('client', null, array('label' => 'Client'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->tab('Dades')->with('')
    ->add('empresa', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Empreses',
     'property' => 'nom',
     'label' => 'Empresa',
     'required' => true
    ))
    ->add('client', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Clients',
     'property' => 'nom',
     'label' => 'Client',
     'required' => true
    ))
    ->add('clientfact', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Clients',
     'property' => 'nom',
     'label' => 'Client a facturar',
     'required' => true
    ))
    ->add('refclient', 'text', array('label' => 'Referencia Client', 'required' => false))
    ->add('descripcio', 'text', array('label' => 'Descripció', 'required' => false))
    ->add('tipotreb', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Tipotrebs',
     'property' => 'nom',
     'label' => 'tipo treball',
     'required' => true
    ))
    ->add('estat', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetEstatsPressupostos(),
					'required' => true,
					'label' => 'Estat',
					'attr' => array('style' => 'width:100%;')))

    ->add('numpres', 'text', array('label' => 'Num Pressupost', 'required' => false, 'read_only' => true))
    ->add('tnumpres', 'text', array('label' => 'Num Pressupost complet', 'required' => false, 'read_only' => true))

    ->add('numfact', 'text', array('label' => 'Num Factura', 'required' => false, 'read_only' => true))
    ->add('tnumpres', 'text', array('label' => 'Num Factura complet', 'required' => false, 'read_only' => true))

    ->add('datafact', 'datetime', array('label' => 'Data factura', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => false, 'read_only' => true,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
    ->add('dataentrega', 'datetime', array('label' => 'Data entrega', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => false, 'read_only' => true,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
    ->add('datafactpresentada', 'datetime', array('label' => 'Data factura presentada', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => false, 'read_only' => true,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
    ->add('datacobro', 'datetime', array('label' => 'Data cobrada', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => false, 'read_only' => true,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))

    ->add('notes', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
    ->add('notesfact', 'textarea', array('label' => 'Notes factura', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))

    ->add('client.tipoclient', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTipoClient(),
					'required' => false,
     'read_only' => true,
					'label' => 'Tipo client',
					'attr' => array('style' => 'width:100%;')))
     ->add('tconta', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTContaFactures(),
					'required' => true,
					'label' => 'F/R',
					'attr' => array('style' => 'width:100%;')))
     ->add('ptancat', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTipoPressupost(),
					'required' => true,
					'label' => 'Pressupost tancat o obert',
					'attr' => array('style' => 'width:100%;')))
     ->add('tipoiva', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTipoIva(),
					'required' => true,
					'label' => 'Tipo iva',
					'attr' => array('style' => 'width:100%;')))

     ->add('comisio', 'text', array('label' => 'comisio', 'required' => true))
     ->add('sumfact', 'text', array('label' => 'Subtotal', 'required' => false, 'read_only' => true))
     ->add('ivafact', 'text', array('label' => 'Iva', 'required' => false, 'read_only' => true))
     ->add('totalfact', 'text', array('label' => 'Total', 'required' => false, 'read_only' => true))

     ->add('totaldeute', 'text', array('label' => 'totaldeute', 'required' => false, 'read_only' => true))
     ->add('acompte', 'text', array('label' => 'acompte', 'required' => false))
     ->add('sumadeute', 'text', array('label' => 'sumadeute', 'required' => false, 'read_only' => true))
   ->end()->end()

   ->tab('Productes')->with('')
    ->add('prodxfact', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Productes'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.prodxfactura'))
				->add('totalpres', 'text', array('label' => 'Total pressupost', 'required' => false, 'read_only' => true))
				->add('totaltreb', 'text', array('label' => 'Total pressupost', 'required' => false, 'read_only' => true))
   ->end()->end()

   ->tab('Partes treball')->with('')
   ->end()->end()

   ->tab('Comptes ??')->with('')
   ->end()->end()

   ->tab('Descripció treball')->with('')
    ->add('info', 'textarea', array('label' => 'Info', 'required' => false, 'attr' => array('rows' => '24', 'style' => 'width:100%;')))
   ->end()->end()

   ->tab('Imatges')->with('')
    ->add('fotofactura', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Imatges'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.fotos'))
   ->end()->end()
  ;        
 }

	public function prePersist($object){
		$this->SaveObject($object);
	}

	public function preUpdate($object){
		$this->SaveObject($object);
	}

	protected function SaveObject($object){
		$this->saveFile($object);

		$em = $this->getModelManager();

		//productes
		$data = $object->getProdxfact();
		foreach($data as $dat){
			$dat->setFactura($object);

			//productes unics
			$prod = $dat->getProducte();
			if($prod == null){
				$p = $em->find('Agui\GestBundle\Entity\Productes', '-100');
				$dat->setProducte($p);
			}
		}

		$object->CalculTotalsFactura();
	}

	public function saveFile($object) {
		$em = $this->getModelManager();
		$attmanager = $this->getConfigurationPool()->getContainer()->get('Attachmentmanager');

		foreach ($object->getFotofactura() as $attach) {
			$attach->setFactura($object);

			if($attach->getId() == null) {
				$em->create($attach);
			}

			$attmanager->upload($attach);
		}
	}

}