<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ConfigAdmin extends Admin {
	protected $baseRoutePattern = 'config';
 protected $baseRouteName = 'config';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('id')
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('tempsmin', 'text', array('label' => 'Temps minim preparacions', 'required' => true))
   ->add('ruta1', 'text', array('label' => 'Ruta copia seguretat 1', 'required' => false))
   ->add('ruta2', 'text', array('label' => 'Ruta copia seguretat 2', 'required' => false))
   ->add('ivadef', 'text', array('label' => 'Iva per defecte', 'required' => true))
   ->add('phoresdef', 'text', array('label' => 'Preu hores no pressupost', 'required' => true))
   ->add('dieslimpres', 'text', array('label' => 'Dies validesa pressupost', 'required' => true))
   ->add('diesvenfact', 'text', array('label' => 'Dies venciment factura', 'required' => true))  
  ;        
 }

}