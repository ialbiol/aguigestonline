<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class CompnumsAdmin extends Admin {
	protected $baseRoutePattern = 'altrescompres';
 protected $baseRouteName = 'altrescompres';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'DESC',
		'_sort_by' => 'data'
	);

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
			->andWhere("o.data >= '" . $maininfo->anyActualInici . "' AND o.data <= '" . $maininfo->anyActualFinal ."'");

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('numfact', null, array('label' => 'Num factura'))
   ->addIdentifier('descripcio', null, array('label' => 'Concepte'))
   ->addIdentifier('data', 'datetime', array('label' => 'Data',	'format' => 'dd/MM/yyyy',))
   ->addIdentifier('total', null, array('label' => 'Total'))
   ->addIdentifier('datapagada', 'datetime', array('label' => 'Data pagada',	'format' => 'dd/MM/yyyy',))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('descripcio', null, array('label' => 'Concepte'))
   ->add('numfact', null, array('label' => 'Num factura'))
   ->add('estat', null, array('label' => 'Estat'), 'choice', array('choices' => Auxselects::GetEstatusFactCompra()))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->tab('Dades')->with('')
    ->add('empresa', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Empreses',
     'property' => 'nom',
     'label' => 'Empresa',
     'required' => true
    ))
    ->add('idllibre', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetLlibre(),
					'required' => true,
					'label' => 'Posicio llibre comptabilitat',
					'attr' => array('style' => 'width:100%;')))
    ->add('proveedor', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Proveedors',
     'property' => 'nom',
     'label' => 'Proveedor',
     'required' => true
    ))
    ->add('data', 'datetime', array('label' => 'Data factura', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => true,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
    ->add('datapagada', 'datetime', array('label' => 'Data factura pagada', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => false,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
    ->add('descripcio', 'text', array('label' => 'descripcio', 'required' => false))
    ->add('naturalesa', 'text', array('label' => 'naturalesa', 'required' => false))
    ->add('numfact', 'text', array('label' => 'Num factura', 'required' => true))
    ->add('estat', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetEstatusFactCompra(),
					'required' => true,
					'label' => 'Estat',
					'attr' => array('style' => 'width:100%;')))
    ->add('tconta', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTContaFactures(),
					'required' => true,
					'label' => 'F / R',
					'attr' => array('style' => 'width:100%;')))
    ->add('notes', 'textarea', array('label' => 'notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
    ->add('tipodte', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTipoDte(),
					'required' => true,
					'label' => 'Tipo descompte',
					'attr' => array('style' => 'width:100%;')))
    ->add('tipoiva', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTipoIva(),
					'required' => true,
					'label' => 'Tipo iva',
					'attr' => array('style' => 'width:100%;')))
    ->add('sum', 'text', array('label' => 'Suma', 'read_only' => true,'required' => false))
    ->add('dte', 'text', array('label' => 'Descompte', 'read_only' => true,'required' => false))
    ->add('iva', 'text', array('label' => 'Iva', 'read_only' => true,'required' => false))
    ->add('total', 'text', array('label' => 'Total', 'read_only' => true,'required' => false))
    ->add('pendent', 'text', array('label' => 'Pendent', 'read_only' => true,'required' => false))
    ->add('acompte', 'text', array('label' => 'A compte', 'read_only' => true,'required' => false))
   ->end()->end()

   ->tab('Productes')->with('')
    ->add('prodxcompra', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Productes'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.prodxcompres'))
   ->end()->end()
  ;        
 }

	public function prePersist($object){
		$this->SaveObject($object);
	}

	public function preUpdate($object){
		$this->SaveObject($object);
	}

	protected function SaveObject($object){
		//productes
		$data = $object->getProdxcompra();
		foreach($data as $dat){
			$dat->setCompnum($object);

			//control stock
			if($dat->getProducte()->getTipoprod()->getId() > 1){ //hores i despeses no
				$oldQuant = 0;
				if($dat->getId() != null){ //comprovo que no haigue cambiat la quantitat
					$em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
					$dql = "SELECT pc.quantitat FROM AguiGestBundle:Prodxcompres pc WHERE pc.id = " . $dat->getId();
					$query = $em->createQuery($dql);
					$olddat = $query->getResult();
					$oldQuant = $olddat[0]["quantitat"];
				}

				$incQ = $dat->getQuantitat() - $oldQuant;

				if($incQ != 0){ //canvi d'estock
					$actq = $dat->getProducte()->getQalmacen() + $incQ;
					$dat->getProducte()->setQalmacen($actq);
				}
			}
		}

		$object->CalculTotals();
	}

}