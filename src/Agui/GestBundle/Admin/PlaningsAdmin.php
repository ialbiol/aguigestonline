<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class PlaningsAdmin extends Admin {
	protected $baseRoutePattern = 'planings';
 protected $baseRouteName = 'planings';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

	protected $datagridValues = array(
		'_page' => 1,
		'_sort_order' => 'ASC',
		'_sort_by' => 'dia'
	);

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('dia', 'datetime', array('label' => 'Data', 'format' => 'dd/MM/yyyy'))
   ->addIdentifier('hora', null, array('label' => 'Hora'))
			->addIdentifier('duracio', null, array('label' => 'Duracio'))
   ->addIdentifier('nota', null, array('label' => 'Nota'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('dia', null, array('label' => 'Data', 'format' => 'dd/MM/yyyy'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
			->add('dia', 'datetime', array('label' => 'Data', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => true, 'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))

   ->add('hora', 'text', array('label' => 'Hora', 'required' => true))
   ->add('duracio', 'text', array('label' => 'Duració', 'required' => true))
			->add('nota', 'textarea', array('label' => 'Nota', 'required' => true, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
   //->add('estat', 'text', array('label' => 'estat', 'required' => true))
  ;        
 }

}