<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ProdxpressupostAdmin extends Admin {
 protected $baseRouteName = 'prodxpressupost';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom')
  ;
 }

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   //->add('factura','sonata_type_model_list', array('required' => false), array())
   ->add('producte','sonata_type_model_list', array('label' => 'Producte','required' => false), array())
   ->add('nom', 'text', array('label' => 'Nom', 'required' => false))
			->add('cost', 'number', array('label' => 'Preu unitat', 'required' => true))
   ->add('quantitatprepres', 'text', array('label' => 'Quantitat', 'required' => true))
   ->add('quantitatpres', 'text', array('label' => 'Quantitat real', 'required' => false, 'read_only' => true))
   //->add('quantitattreb', 'text', array('label' => 'quantitattreb', 'required' => true))
   //->add('quantitatfact', 'text', array('label' => 'quantitatfact', 'required' => true))
   //->add('pcompra', 'text', array('label' => 'pcompra', 'required' => true))
   ->add('totalpressupost', 'number', array('label' => 'Subtotal', 'required' => false, 'read_only' => true))
   ->add('percent', 'number', array('label' => '%', 'required' => false, 'read_only' => true))
   ->add('beneficipressupost', 'number', array('label' => 'Benefici', 'required' => false, 'read_only' => true))
  ;        
 }

}