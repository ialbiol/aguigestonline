<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class ProdxfacturaAdmin extends Admin {
 protected $baseRouteName = 'prodxfactura';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom')
  ;
 }

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   //->add('factura','sonata_type_model_list', array('required' => false), array())
   //->add('quantitatprepres', 'text', array('label' => 'quantitatprepres', 'required' => true))
   ->add('producte','sonata_type_model_list', array('label' => 'Producte','required' => false), array())
   ->add('nom', 'text', array('label' => 'Nom', 'required' => false))
   ->add('cost', 'text', array('label' => 'Preu', 'required' => true))
   ->add('quantitatpres', 'text', array('label' => 'Quantitat pressupost', 'required' => false, 'read_only' => true))
   ->add('totalpressupost', 'text', array('label' => 'Subtotal pressupost', 'required' => false, 'read_only' => true))

   ->add('quantitattreb', 'text', array('label' => 'Quantitat treball', 'required' => false, 'read_only' => true))
   ->add('totaltreball', 'text', array('label' => 'Subtotal treball', 'required' => false, 'read_only' => true))

   ->add('quantitatfact', 'text', array('label' => 'Quantitat factura', 'required' => true))

   //->add('pcompra', 'text', array('label' => 'pcompra', 'required' => true))
   ->add('totalfactura', 'text', array('label' => 'Subtotal factura', 'required' => false, 'read_only' => true))
   
  ;        
 }

}