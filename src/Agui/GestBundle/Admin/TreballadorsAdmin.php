<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Agui\GestBundle\Entity\Auxselects;

class TreballadorsAdmin extends Admin {
	protected $baseRoutePattern = 'treballadors';
 protected $baseRouteName = 'treballadors';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Nom'))
   ->addIdentifier('telf', null, array('label' => 'Telefono'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Nom'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
			->tab('Dades')->with('')
				->add('empresa', 'entity', array(
						'attr' => array('class' => 'select2'),
						'class' => 'Agui\GestBundle\Entity\Empreses',
						'property' => 'nom',
						'label' => 'Empresa',
						'required' => true
					))
				->add('nom', 'text', array('label' => 'Nom', 'required' => true))
				->add('direccio', 'text', array('label' => 'Direcció', 'required' => false))
				->add('telf', 'text', array('label' => 'Telefono', 'required' => false))
				->add('nif', 'text', array('label' => 'Nif', 'required' => false))
				->add('cc', 'text', array('label' => 'Numero de compte', 'required' => false))
				->add('segsocial', 'text', array('label' => 'Numero seguretat social', 'required' => false))
				->add('actiu', 'sonata_type_translatable_choice', array(
						'choices' => Auxselects::GetTreballadorActiu(),
						'required' => true,
						'label' => 'Actiu?',
						'attr' => array('style' => 'width:100%;')))
				//->add('foto', 'text', array('label' => 'Foto', 'required' => false))
				//->add('color', 'text', array('label' => 'Color', 'required' => false))
				->add('notes', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
			->end()->end()

   ->tab('Imatges')->with('')
				->add('fototreballador', 'sonata_type_collection', array('required' => false,'by_reference' => true,'label' => 'Imatges'), array('edit' => 'inline','inline' => 'table','admin_code' => 'sonata.admin.fotos'))
   ->end()->end()
  ;
 }

	public function prePersist($object){
		$this->SaveObject($object);
	}

	public function preUpdate($object){
		$this->SaveObject($object);
	}

	protected function SaveObject($object){
		$this->saveFile($object);
	}

	public function saveFile($object) {
		$em = $this->getModelManager();
		$attmanager = $this->getConfigurationPool()->getContainer()->get('Attachmentmanager');

		foreach ($object->getFototreballador() as $attach) {
			$attach->setTreballador($object);

			if($attach->getId() == null) {
				$em->create($attach);
			}

			$attmanager->upload($attach);
		}
	}

}