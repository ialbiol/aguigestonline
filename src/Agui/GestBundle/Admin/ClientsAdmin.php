<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class ClientsAdmin extends Admin {
	protected $baseRoutePattern = 'clients';
 protected $baseRouteName = 'clients';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
		;

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Client'))
   ->addIdentifier('malnom', null, array('label' => 'Mal nom'))
   ->addIdentifier('telf', null, array('label' => 'Telefon 1'))
   ->addIdentifier('telf2', null, array('label' => 'Telefon 3'))
   ->addIdentifier('mobil1', null, array('label' => 'Mobil 1'))
   ->addIdentifier('mobil2', null, array('label' => 'Mobil 2'))
   ->addIdentifier('email', null, array('label' => 'Email'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Client'))
   ->add('malnom', null, array('label' => 'Mal nom'))
   ->add('telf', null, array('label' => 'Telefon 1'))
   ->add('localitat', null, array('label' => 'Localitat'))
   ->add('tipoclient', 'doctrine_orm_string', array('label' => 'Particular / empresa'), 'choice', array('choices' => Auxselects::GetTipoClient()))
   ->add('histo', 'doctrine_orm_string', array('label' => 'En us / historic'), 'choice', array('choices' => Auxselects::GetHistoricClient()))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->tab('Dades')->with('')
    ->add('nom', 'text', array('label' => 'Client', 'required' => true))
    ->add('malnom', 'text', array('label' => 'Mal nom', 'required' => false))
    ->add('direccio', 'text', array('label' => 'Direccio', 'required' => false))
    ->add('localitat', 'text', array('label' => 'Localitat', 'required' => false))
    ->add('telf', 'text', array('label' => 'telefono', 'required' => false))
    ->add('telf2', 'text', array('label' => 'telf2', 'required' => false))
    ->add('telf3', 'text', array('label' => 'telf3', 'required' => false))
    ->add('telf4', 'text', array('label' => 'telf4', 'required' => false))
    ->add('mobil1', 'text', array('label' => 'mobil1', 'required' => false))
    ->add('mobil2', 'text', array('label' => 'mobil2', 'required' => false))
    ->add('mobil3', 'text', array('label' => 'mobil3', 'required' => false))
    ->add('mobil4', 'text', array('label' => 'mobil4', 'required' => false))
    ->add('email', 'text', array('label' => 'email', 'required' => false))
    ->add('nif', 'text', array('label' => 'nif', 'required' => false))
    ->add('ncompte', 'text', array('label' => 'ncompte', 'required' => false))
    ->add('notes', 'textarea', array('label' => 'notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
    ->add('tipoclient', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetTipoClient(),
					'required' => true,
					'label' => 'Particular / empresa',
					'attr' => array('style' => 'width:100%;')))
    ->add('histo', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetHistoricClient(),
					'required' => true,
					'label' => 'En us / historic',
					'attr' => array('style' => 'width:100%;')))
    //->add('percent', 'text', array('label' => 'percent', 'required' => false))
   ->end()->end()

   ->tab('Pressupostos')->with('')
   ->end()->end()

   ->tab('Pressupostos oberts')->with('')
   ->end()->end()

   ->tab('Treballs')->with('')
   ->end()->end()

   ->tab('Factures')->with('')
   ->end()->end()

   ->tab('Plantilles impreses')->with('')
   ->end()->end()

   ->tab('Notes')->with('')
    ->add('info', 'textarea', array('label' => 'info', 'required' => false, 'attr' => array('rows' => '24', 'style' => 'width:100%;')))
   ->end()->end()

   ->tab('Notes colors')->with('')
    ->add('formules', 'textarea', array('label' => 'formules', 'required' => false, 'attr' => array('rows' => '24', 'style' => 'width:100%;')))
   ->end()->end()

   ->tab('Notes tints')->with('')
    ->add('ntints', 'textarea', array('label' => 'Notes tints', 'required' => false, 'attr' => array('rows' => '24', 'style' => 'width:100%;')))
   ->end()->end()
  ;        
 }

}