<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class EmpresesAdmin extends Admin {
	protected $baseRoutePattern = 'empreses';
 protected $baseRouteName = 'empreses';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom')
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom')
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('nom', 'text', array('label' => 'nom', 'required' => true))
  ;        
 }

}