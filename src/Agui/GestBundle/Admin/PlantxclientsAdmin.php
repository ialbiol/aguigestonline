<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class PlantxclientsAdmin extends Admin {
 protected $baseRouteName = 'plantxclients';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
			->add('client','sonata_type_model_list', array('label' => 'Client','required' => true), array())
			->add('data', 'datetime', array('label' => 'Data', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => false,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
  ;        
 }

}