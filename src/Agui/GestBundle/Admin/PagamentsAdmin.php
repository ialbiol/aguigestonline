<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class PagamentsAdmin extends Admin {
 protected $baseRouteName = 'pagaments';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('data', 'datetime', array('label' => 'Data', 'widget' => 'single_text',	'format' => 'dd/MM/yyyy',	'required' => true,	'attr' => array('class' => 'datepicker',	'style' => 'width:80px')))
   ->add('quantitat', 'text', array('label' => 'Quantitat', 'required' => true))
   ->add('tpagament', 'text', array('label' => 'Tipo pagament', 'required' => true))  
  ;        
 }

}