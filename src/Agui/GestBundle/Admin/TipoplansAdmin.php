<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class TipoplansAdmin extends Admin {
	protected $baseRoutePattern = 'tipoplans';
 protected $baseRouteName = 'tipoplans';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;
	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

	public function createQuery($context = 'list') {
		$query = parent::createQuery($context);

		$user = $this->getSecurityContext()->getToken()->getUser();
		$maininfo = $this->getConfigurationPool()->getContainer()->get('Maininfo');
		$maininfo->setFilters($user, $this->session);
		//echo $maininfo->IDempresaActual; die;

		$query = $query
			->andWhere("o.empresa = " . $maininfo->IDempresaActual)
		;

		return new ProxyQuery($query);
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Nom'))
			->addIdentifier('tipotext', null, array('label' => 'Tipo'))
			->addIdentifier('modeltext', null, array('label' => 'Model'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Nom'))
			->add('tipo', null, array('label' => 'Tipo'), 'choice', array('choices' => Auxselects::GetPlantillesClasesTipo()))
			->add('idtoca', null, array('label' => 'Models'), 'choice', array('choices' => Auxselects::GetPlantillesModels()))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
			->add('empresa', 'entity', array(
     'attr' => array('class' => 'select2'),
     'class' => 'Agui\GestBundle\Entity\Empreses',
     'property' => 'nom',
     'label' => 'Empresa',
     'required' => true
    ))
   ->add('nom', 'text', array('label' => 'nom', 'required' => true))
			->add('tipo', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetPlantillesClasesTipo(),
					'required' => true,
					'label' => 'Tipo',
					'attr' => array('style' => 'width:100%;')))
			->add('idtoca', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetPlantillesModels(),
					'required' => true,
					'label' => 'Model',
					'attr' => array('style' => 'width:100%;')))
  ;        
 }

}