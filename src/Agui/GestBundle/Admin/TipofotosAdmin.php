<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;
use Agui\GestBundle\Entity\Auxselects;

class TipofotosAdmin extends Admin {
	protected $baseRoutePattern = 'tipofotos';
 protected $baseRouteName = 'tipofotos';

 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

	protected $session;
	protected $securityContext;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getSession(){
		return $this->session;
	}

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Nom'))
			->addIdentifier('gruptext', null, array('label' => 'Grup'))
			->addIdentifier('orden', null, array('label' => 'Orden'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Nom'))
			->add('grup', null, array('label' => 'Grup'), 'choice', array('choices' => Auxselects::GetGrupFotos()))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('nom', 'text', array('label' => 'nom', 'required' => true))
			->add('grup', 'sonata_type_translatable_choice', array(
					'choices' => Auxselects::GetGrupFotos(),
					'required' => true,
					'label' => 'Grup',
					'attr' => array('style' => 'width:100%;')))
			->add('orden', 'text', array('label' => 'Orden', 'required' => true))
  ;        
 }

}