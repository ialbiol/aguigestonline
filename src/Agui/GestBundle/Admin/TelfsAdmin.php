<?php

namespace Agui\GestBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;

class TelfsAdmin extends Admin {
	protected $baseRoutePattern = 'telfs';
 protected $baseRouteName = 'telfs';
 protected $maxPerPage = 15;
 protected $maxPageLinks = 10;

 /**
 * {@inheritdoc}
 */
 protected function configureListFields(ListMapper $listMapper) {
  $listMapper
   ->addIdentifier('nom', null, array('label' => 'Nom'))
   ->addIdentifier('malnom', null, array('label' => 'Mal nom'))
   ->addIdentifier('telf1', null, array('label' => 'Telefono'))
   ->addIdentifier('telf2', null, array('label' => 'Telefono 2'))
   ->addIdentifier('mobil1', null, array('label' => 'Mobil'))
   ->addIdentifier('mobil2', null, array('label' => 'Mobil 2'))
   ->addIdentifier('email', null, array('label' => 'Email'))
  ;
 }

 /**
 * {@inheritdoc}
 */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
  $filterMapper
   ->add('nom', null, array('label' => 'Nom'))
   ->add('malnom', null, array('label' => 'Mal nom'))
   ->add('telf1', null, array('label' => 'Telefono'))
			->add('mobil1', null, array('label' => 'mobil'))
  ;
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureFormFields(FormMapper $formMapper) {
  $formMapper
   ->add('nom', 'text', array('label' => 'Nom', 'required' => false))
			->add('malnom', 'text', array('label' => 'Mal nom', 'required' => false))
   ->add('direccio', 'text', array('label' => 'Direcció', 'required' => false))
   ->add('email', 'text', array('label' => 'Email', 'required' => false))
   ->add('telf1', 'text', array('label' => 'Telefono 1', 'required' => false))
   ->add('telf2', 'text', array('label' => 'Telefono 2', 'required' => false))
			->add('telf3', 'text', array('label' => 'Telefono 3', 'required' => false))
   ->add('telf4', 'text', array('label' => 'Telefono 4', 'required' => false))
   ->add('mobil1', 'text', array('label' => 'Mobil 1', 'required' => false))
   ->add('mobil2', 'text', array('label' => 'Mobil 2', 'required' => false))
   ->add('mobil3', 'text', array('label' => 'Mobil 3', 'required' => false))
   ->add('mobil4', 'text', array('label' => 'Mobil 4', 'required' => false))
			->add('notes', 'textarea', array('label' => 'Notes', 'required' => false, 'attr' => array('rows' => '8', 'style' => 'width:100%;')))
  ;
 }

}