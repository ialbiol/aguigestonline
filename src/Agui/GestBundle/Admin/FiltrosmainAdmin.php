<?php

namespace Agui\GestBundle\Admin;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Model\UserInterface;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

class FiltrosmainAdmin extends Admin {
 protected $baseRouteName = 'filtrosmain';
 protected $baseRoutePattern = 'filtrosmain';
 protected $maxPageLinks = 10;
 protected $maxPerPage = 10;

	public function setSecurityContext(SecurityContextInterface $securityContext) {
		$this->securityContext = $securityContext;
	}

	public function getSecurityContext() {
		return $this->securityContext;
	}

 protected function configureRoutes(RouteCollection $collection) {
  //$collection->add('doformprint');
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureListFields(ListMapper $listMapper) {
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureDatagridFilters(DatagridMapper $filterMapper) {
 }
 
 /**
  * {@inheritdoc}
  */
 protected function configureShowFields(ShowMapper $showMapper) {
 }

}