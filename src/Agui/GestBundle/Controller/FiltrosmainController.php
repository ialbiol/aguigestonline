<?php

namespace Agui\GestBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;
use Agui\GestBundle\Entity\Auxselects;

class FiltrosmainController extends Controller {

	public function showAction($id = NULL) {
		$request = $this->get('request');

		return $this->getValidations($request);
	}

	public function listAction() {
		$request = $this->get('request');
		return $this->getValidations($request);
	}

	public function getValidations(Request $request) {
		$session = $request->getSession();

		$fil = new \Agui\GestBundle\Entity\Filtrosmain();

		$filters = null;
		if ($session->has('main_filters')) { $filters = $session->get('main_filters'); }

	 //busco les empreses disponibles per al usuari
	 $user = $this->admin->getSecurityContext()->getToken()->getUser();
	 $maininfo = $this->get('Maininfo');
	 $empreses = $maininfo->getEmpresesUser($user, $session);
		$fil->setEmpresa($maininfo->getIDEmpresaActual($session));

	 //busco els anys disponibles de l'empresa seleccionada
	 $anys = $maininfo->getAnysFiltro($session);
		$fil->setAny($maininfo->getAnyActual($session));

		//busco si hi ha algun client filtrat
		$fil->setClient($maininfo->getClientActual($session));
		//echo '<pre>'.\Doctrine\Common\Util\Debug::dump($fil).'</pre>';die;

		$form = $this->createFormBuilder($fil)
		 ->add('empresa', 'sonata_type_translatable_choice', array(
				//'data' => $fil->getEmpresa(),
			 'choices' => $empreses,
			 'required' => false,
			 'label' => 'Empresa',
			 'attr' => array('style' => 'width:100%;', 'placeholder' => 'empresa')))
		 ->add('any', 'sonata_type_translatable_choice', array(
				//'data' => $fil->getAny(),
			 'choices' => $anys,
			 'required' => false,
			 'label' => 'Anys',
			 'attr' => array('style' => 'width:100%;', 'placeholder' => 'any')))
			->add('client', 'entity', array('class' => 'Agui\GestBundle\Entity\Clients', 'query_builder' => function(EntityRepository $er) {
					return $er->createQueryBuilder('u')
							->where('u.histo = :histo')
							->orderby('u.nom')
							->setParameter('histo', 0);
				},
				'property' => 'nom', 'required' => false,
				//'data' => $fil->getClient(),
				'attr' => array('style' => 'width: 100%; padding-bottom:8px;', 'placeholder' => 'client'), 'label' => 'Client'))
		 //->add('find', 'submit')
		 ->getForm();

		if ($request->isMethod('POST')) {
			$form->bind($request);

			if ($form->isValid()) {
				$filters = $form->getData();
				$session->set('main_filters', $filters);
				$url = $this->getRequest()->headers->get('referer');
				return $this->redirect($url);
			}
		}

		return $this->render('AguiGestBundle:Default:FiltrosMain.html.twig', array(
			'form' => $form->createView(),
		));
	}

}