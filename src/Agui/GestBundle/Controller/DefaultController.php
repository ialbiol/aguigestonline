<?php
namespace Agui\GestBundle\Controller;

//use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DefaultController extends Controller {

	public function getimageAction($img, $tipo, $tamany) {
		return $this->render('AguiGestBundle:Block:block_get_image.html.twig', array('img' => $img, 'tipo' => $tipo, 'tamany' => $tamany));
	}

	public function downloadattachmentAction($id = null, $tipo = 0) {
		if ($id != null) {

		$attachment = $this->getDoctrine()->getRepository("AguiGestBundle:Fotos")->find($id);
		$path = '../media/'.$attachment->getImagenamewpath();
		$filename = $attachment->getNom();

			$response = new Response();
			//set headers
			$response->headers->set('Cache-Control', 'private');
			//$response->headers->set('Content-Type', mime_content_type($path).'; charset=utf-8');
			$response->headers->set('Content-Type', 'image/jpeg'.'; charset=utf-8');
			$response->headers->set('Content-Disposition', 'attachment;filename="'.$filename.'"');
			// If you are using a https connection, you have to set those two headers and use sendHeaders() for compatibility with IE <9
			$response->headers->set('Pragma', 'public');
			$response->headers->set('Cache-Control', 'maxage=1');
			$response->headers->set('Content-length', filesize($path));

			$response->sendHeaders();
			$response->setContent(readfile($path));

			return $response;
		} else { throw new AccessDeniedException(); }
	}

}