<?php
namespace Agui\GestBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class Maininfo { // extends Controller {
	protected $doctrine;
	protected $em;

 public $empreses = array(); //llistat empreses del usuari
 public $IDempresaActual = 0; //id de l'empresa actual
 public $anysFiltro = array(); //llistat anys info de l'empresa
 public $anyActual = ""; //any actual del filtro
 public $anyActualInici = ""; //timestamp de l'inici de l'any actual
 public $anyActualFinal = "";//timestamp del final de l'any actual
	public $clientActual = null; //filtro per client. per defecte no hi ha cap filtro

	function __construct($doctrine) {
		set_time_limit(3600);
		$this->doctrine = $doctrine;
		$this->em = $this->doctrine->getManager();

	 $this->anyActual = date("Y");
	}

	public function getEmpresesUser($user = NULL, Session $session = NULL) {
	 if($user == NULL){ return $this->empreses; } //seguretat

		$dql = "SELECT e.id, e.nom FROM AguiGestBundle:Empresaxusers c JOIN c.empresa e WHERE c.user = " . $user->getId() . " AND e.id>0 ORDER BY c.id ASC";
		$query = $this->em->createQuery($dql);
		$empresesdb = $query->getResult();
	 //echo '<pre>'.\Doctrine\Common\Util\Debug::dump($empresesdb).'</pre>';die;

	 foreach($empresesdb as $empresa){
		 $this->empreses[$empresa["id"]] = $empresa["nom"];
	 }
	 //echo '<pre>'.print_r($this->empreses, true).'</pre>'; //die;

	 $this->getIDEmpresaActual($session);
	 return $this->empreses;
	}

	public function getIDEmpresaActual(Session $session = NULL) {
	 if($session == NULL){
		 if($this->empreses == NULL){
			 //echo "error";
			 $this->IDempresaActual = 0;//seguretat. no hauria de pasar mai
		 } else {
			 //no hi ha encara res seleccionat. selecciono la primera
			 //echo "ho intento";
			 $empresa = array_keys($this->empreses);
			 //echo '<pre>'.print_r($empresa, true).'</pre>'; die;
			 $this->IDempresaActual = $empresa[0];
		 }
	 } else {
		 //busco al request
		 //echo "tinc";
		 if ($session->has('main_filters')) {
			 //si hi ha una seleccionada al request
			 $filters = $session->get('main_filters');
			 //$this->IDempresaActual = $filters["empresa"];
				$this->IDempresaActual = $filters->getEmpresa();
		 } else {
			 //si no hi ha cap seleccionada al request
			 $empresa = array_keys($this->empreses);
			 //echo '<pre>'.print_r($empresa, true).'</pre>'; die;
			 $this->IDempresaActual = $empresa[0];
		 }
	 }

		//hiper seguretat
		if($this->IDempresaActual == ""){ $this->IDempresaActual = $empresa[0]; }

	 //echo '<pre>'.print_r($this->IDempresaActual, true).'</pre>'; die;
	 return $this->IDempresaActual;
	}

	public function getAnysFiltro(Session $session = NULL) {
	 if($this->IDempresaActual == 0){ return $this->anysFiltro; } //seguretat

	 $con = $this->em->getConnection();

	 //compte en los camps de les dates. estan repes. data i ddata. aqui usar ddata perque es query directa
		$sql = "SELECT DISTINCT YEAR(ddatapres) as 'any' FROM Factures WHERE idempresa = " . $this->IDempresaActual . " AND ddatapres > 0 ORDER BY ddatapres DESC";
	 //echo '<pre>'.$sql.'</pre>';die;
		$st = $con->prepare($sql);
	 $st->execute();
		$anysdb = $st->fetchAll();
	 //echo '<pre>'.\Doctrine\Common\Util\Debug::dump($anysdb).'</pre>';die;

	 foreach($anysdb as $any){
		 $this->anysFiltro[$any["any"]] = $any["any"];
	 }
	 //echo '<pre>'.print_r($this->anysFiltro, true).'</pre>'; die;

	 $this->getAnyActual($session);
	 return $this->anysFiltro;
	}

	public function getAnyActual(Session $session = NULL) {
	 if($session == NULL){
		 if($this->anysFiltro == NULL){
			 //echo "error";
			 $this->anyActual = date("Y");//seguretat. no hauria de pasar mai
		 } else {
			 //no hi ha encara res seleccionat. selecciono la primera
			 //echo "ho intento";
			 $any = array_keys($this->anysFiltro);
			 //echo '<pre>'.print_r($any, true).'</pre>'; die;
			 $this->anyActual = $any[0];
		 }
	 } else {
		 //busco al request
		 //echo "tinc";
		 if ($session->has('main_filters')) {
			 //si hi ha una seleccionada al request
			 $filters = $session->get('main_filters');
			 //$this->anyActual = $filters["any"];
				$this->anyActual = $filters->getAny();
		 } else {
			 //si no hi ha cap seleccionada al request
			 $any = array_keys($this->anysFiltro);
			 //echo '<pre>'.print_r($any, true).'</pre>'; die;
			 $this->anyActual = $any[0];
		 }
	 }

		//hiper seguretat
		if($this->anyActual == ""){ $this->anyActual = date("Y"); }

	 //echo '<pre>'.print_r($this->anyActual, true).'</pre>'; die;
	 $this->getIniciAnyActual();
	 $this->getFinalAnyActual();
	 return $this->anyActual;
	}

 public function getIniciAnyActual(){
	 //$this->anyActualInici = mktime(0, 0, 0, 1, 1, $this->anyActual);
		$this->anyActualInici = $this->anyActual . "-01-01";
	 return $this->anyActualInici;
 }

 public function getFinalAnyActual(){
	 //$this->anyActualFinal = mktime(23, 59, 59, 12, 31, $this->anyActual);
		$this->anyActualFinal = $this->anyActual . "-12-31";
	 return $this->anyActualFinal;
 }

	public function getClientActual(Session $session = NULL){
		//busco si hi ha algun filtro de client
	 if ($session->has('main_filters')) {
		 //si hi ha una seleccionada al request
		 $filters = $session->get('main_filters');
		 //$this->clientActual = $filters["client"];
			if($filters->getClient() == null){
				$this->clientActual = null;
			} else {
				$this->clientActual = $filters->getClient()->getId();
			}
	 } else {
			//si no hi ha cap seleccionada al request
			$this->clientActual = null;
		}

		return $this->clientActual;
	}

	public function setFilters($user = NULL, Session $session = NULL){
		//buso empresa actual
		$this->getEmpresesUser($user, $session);

		//busco any actual
		$this->getAnysFiltro($session);

		//busco si hi ha algun filtro de client
		$this->getClientActual($session);
	}

}