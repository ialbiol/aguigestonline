<?php

namespace Agui\GestBundle\Services;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityRepository;

class Attachmentmanager {

	private $container;
	private $em;
	private $pathmedia;
	private $pathcache;

	public function __construct($container, $pathmedia) {
		$this->container = $container;
		$this->em = $this->container->get('doctrine')->getManager();

		$this->pathmedia = $pathmedia;
		$this->pathcache = 'media/cache/';
	}


	public function ClearCache($folder = "", $filename) {
		$filters = array('low_thumb', 'mid_thumb', 'high_thumb');
		$imagepath = $folder . '/' . $filename;
		foreach ($filters as $filter) {
			//Remove from cache
			$cacheManager = $this->container->get('liip_imagine.cache.manager');
			$cacheManager->remove($imagepath, $filter);

			//Regenerate
			$imagemanagerResponse = $this->container->get('liip_imagine.controller')
					->filterAction($this->container->get('request'), $imagepath, $filter);
		}
	}


	//upload. usar esta funcio al pujar una foto desde un formulari
	public function upload($attachment) {
		if (null === $attachment->getFile()) {
			return;
		}

		$filetype = $attachment->getTipofoto();
		$filegrup = $filetype->getGrup();

		// Imatges productes
		if($filegrup == 1){ //images
			$this->uploadImagesProductes($attachment);
			return;
		}

		// Imatges plantilles
		if($filegrup == 10){ //images
			$this->uploadImagesPlantilles($attachment);
			return;
		}

		// Imatges treballs
		if($filegrup == 20){ //images
			$this->uploadImagesTreballs($attachment);
			return;
		}

		// Imatges treballadors
		if($filegrup == 30){ //images
			$this->uploadImagesTreballadors($attachment);
			return;
		}

	}

	public function uploadImagesProductes($attachment) {
		$filetype = $attachment->getTipofoto();
		$filegrup = $filetype->getGrup();

		$id = str_pad($attachment->getProducte()->getId(), 6, "0", STR_PAD_LEFT);

		$filename = $id . '-' . $attachment->getId() . '.' . $attachment->getFile()->guessExtension();
		$path = $this->pathmedia . 'productes/';

		$filepath = $path . $filename;

			//Remove old image
			if(isset($filename)) {
				if(file_exists($filepath)) {
					unlink($filepath);
				}
			}

			if (is_array($filename) == false) {
				//set imagename and move the file to it's path
				$attachment->setNom($filename);
				$attachment->setRuta('productes/');
				$attachment->getFile()->move($path, $filename);

				//remove cached image
				$this->ClearCache("productes", $filename);
			}

		$attachment->setFile(null);
	}

	public function uploadImagesPlantilles($attachment) {
		$filetype = $attachment->getTipofoto();
		$filegrup = $filetype->getGrup();

		$id = str_pad($attachment->getPlantilla()->getId(), 6, "0", STR_PAD_LEFT);

		$filename = $id . '-' . $attachment->getId() . '.' . $attachment->getFile()->guessExtension();
		$path = $this->pathmedia . 'plantilles/';

		$filepath = $path . $filename;

			//Remove old image
			if(isset($filename)) {
				if(file_exists($filepath)) {
					unlink($filepath);
				}
			}

			if (is_array($filename) == false) {
				//set imagename and move the file to it's path
				$attachment->setNom($filename);
				$attachment->setRuta('plantilles/');
				$attachment->getFile()->move($path, $filename);

				//remove cached image
				$this->ClearCache("plantilles", $filename);
			}

		$attachment->setFile(null);
	}

	public function uploadImagesTreballs($attachment) {
		$filetype = $attachment->getTipofoto();
		$filegrup = $filetype->getGrup();

		$id = str_pad($attachment->getFactura()->getId(), 6, "0", STR_PAD_LEFT);

		$filename = $id . '-' . $attachment->getId() . '.' . $attachment->getFile()->guessExtension();
		$path = $this->pathmedia . 'treballs/';

		$filepath = $path . $filename;

			//Remove old image
			if(isset($filename)) {
				if(file_exists($filepath)) {
					unlink($filepath);
				}
			}

			if (is_array($filename) == false) {
				//set imagename and move the file to it's path
				$attachment->setNom($filename);
				$attachment->setRuta('treballs/');
				$attachment->getFile()->move($path, $filename);

				//remove cached image
				$this->ClearCache("treballs", $filename);
			}

		$attachment->setFile(null);
	}

	public function uploadImagesTreballadors($attachment) {
		$filetype = $attachment->getTipofoto();
		$filegrup = $filetype->getGrup();

		$id = str_pad($attachment->getTreballador()->getId(), 6, "0", STR_PAD_LEFT);

		$filename = $id . '-' . $attachment->getId() . '.' . $attachment->getFile()->guessExtension();
		$path = $this->pathmedia . 'treballadors/';

		$filepath = $path . $filename;

			//Remove old image
			if(isset($filename)) {
				if(file_exists($filepath)) {
					unlink($filepath);
				}
			}

			if (is_array($filename) == false) {
				//set imagename and move the file to it's path
				$attachment->setNom($filename);
				$attachment->setRuta('treballadors/');
				$attachment->getFile()->move($path, $filename);

				//remove cached image
				$this->ClearCache("treballadors", $filename);
			}

		$attachment->setFile(null);
	}

}