<?php
namespace Agui\GestBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class IconType extends AbstractType
{
    
    public function getParent()
    {
        return 'form';
        
    }
    
    public function getName()
    {
        return 'icon';
    }
}

