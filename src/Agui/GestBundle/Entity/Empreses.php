<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Empreses {
	protected $nom;

 protected $id;

 // @var \Doctrine\Common\Collections\Collection
 private $albaran;
 private $client;
 private $compnum;
 private $compra;
 private $factura;
 private $parte;
 private $plantilla;
 private $producte;
 private $proveedor;
 private $tipoplan;
 private $tipoprod;
 private $tipotreb;
 private $treballador;
 private $empresaxuser;

 public function __construct() {
  $this->albaran = new \Doctrine\Common\Collections\ArrayCollection();
  $this->client = new \Doctrine\Common\Collections\ArrayCollection();
  $this->compnum = new \Doctrine\Common\Collections\ArrayCollection();
  $this->compra = new \Doctrine\Common\Collections\ArrayCollection();
  $this->factura = new \Doctrine\Common\Collections\ArrayCollection();
  $this->parte = new \Doctrine\Common\Collections\ArrayCollection();
  $this->plantilla = new \Doctrine\Common\Collections\ArrayCollection();
  $this->producte = new \Doctrine\Common\Collections\ArrayCollection();
  $this->proveedor = new \Doctrine\Common\Collections\ArrayCollection();
  $this->tipoplan = new \Doctrine\Common\Collections\ArrayCollection();
  $this->tipoprod = new \Doctrine\Common\Collections\ArrayCollection();
  $this->tipotreb = new \Doctrine\Common\Collections\ArrayCollection();
  $this->treballador = new \Doctrine\Common\Collections\ArrayCollection();
  $this->empresaxuser = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

	public function getId() {
		return $this->id;
	}

 public function addAlbaran(\Agui\GestBundle\Entity\Albarans $albaran){
  $this->albaran[] = $albaran;
  return $this;
 }

 public function removeAlbaran(\Agui\GestBundle\Entity\Albarans $albaran){
  $this->albaran->removeElement($albaran);
 }

 public function getAlbaran(){
  return $this->albaran;
 }

 public function addClient(\Agui\GestBundle\Entity\Clients $client){
  $this->client[] = $client;
  return $this;
 }

 public function removeClient(\Agui\GestBundle\Entity\Clients $client){
  $this->client->removeElement($client);
 }

 public function getClient(){
  return $this->client;
 }

 public function addCompnum(\Agui\GestBundle\Entity\Compnums $compnum){
  $this->compnum[] = $compnum;
  return $this;
 }

 public function removeCompnum(\Agui\GestBundle\Entity\Compnums $compnum){
  $this->compnum->removeElement($compnum);
 }

 public function getCompnum(){
  return $this->compnum;
 }

 public function addCompra(\Agui\GestBundle\Entity\Compres $compra){
  $this->compra[] = $compra;
  return $this;
 }

 public function removeCompra(\Agui\GestBundle\Entity\Compres $compra){
  $this->compra->removeElement($compra);
 }

 public function getCompra(){
  return $this->compra;
 }

 public function addFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura[] = $factura;
  return $this;
 }

 public function removeFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura->removeElement($factura);
 }

 public function getFactura(){
  return $this->factura;
 }

 public function addParte(\Agui\GestBundle\Entity\Partes $parte){
  $this->parte[] = $parte;
  return $this;
 }

 public function removeParte(\Agui\GestBundle\Entity\Partes $parte){
  $this->parte->removeElement($parte);
 }

 public function getParte(){
  return $this->parte;
 }

 public function addPlantilla(\Agui\GestBundle\Entity\Plantilles $plantilla){
  $this->plantilla[] = $plantilla;
  return $this;
 }

 public function removePlantilla(\Agui\GestBundle\Entity\Plantilles $plantilla){
  $this->plantilla->removeElement($plantilla);
 }

 public function getPlantilla(){
  return $this->plantilla;
 }

 public function addProducte(\Agui\GestBundle\Entity\Productes $producte){
  $this->producte[] = $producte;
  return $this;
 }

 public function removeProducte(\Agui\GestBundle\Entity\Productes $producte){
  $this->producte->removeElement($producte);
 }

 public function getProducte(){
  return $this->producte;
 }

 public function addProveedor(\Agui\GestBundle\Entity\Proveedors $proveedor){
  $this->proveedor[] = $proveedor;
  return $this;
 }

 public function removeProveedor(\Agui\GestBundle\Entity\Proveedors $proveedor){
  $this->proveedor->removeElement($proveedor);
 }

 public function getProveedor(){
  return $this->proveedor;
 }

 public function addTipoplan(\Agui\GestBundle\Entity\Tipoplans $tipoplan){
  $this->tipoplan[] = $tipoplan;
  return $this;
 }

 public function removeTipoplan(\Agui\GestBundle\Entity\Tipoplans $tipoplan){
  $this->tipoplan->removeElement($tipoplan);
 }

 public function getTipoplan(){
  return $this->tipoplan;
 }

 public function addTipoprod(\Agui\GestBundle\Entity\Tipoprods $tipoprod){
  $this->tipoprod[] = $tipoprod;
  return $this;
 }

 public function removeTipoprod(\Agui\GestBundle\Entity\Tipoprods $tipoprod){
  $this->tipoprod->removeElement($tipoprod);
 }

 public function getTipoprod(){
  return $this->tipoprod;
 }

 public function addTipotreb(\Agui\GestBundle\Entity\Tipotrebs $tipotreb){
  $this->tipotreb[] = $tipotreb;
  return $this;
 }

 public function removeTipotreb(\Agui\GestBundle\Entity\Tipotrebs $tipotreb){
  $this->tipotreb->removeElement($tipotreb);
 }

 public function getTipotreb(){
  return $this->tipotreb;
 }

 public function addTreballador(\Agui\GestBundle\Entity\Treballadors $treballador){
  $this->treballador[] = $treballador;
  return $this;
 }

 public function removeTreballador(\Agui\GestBundle\Entity\Treballadors $treballador){
  $this->treballador->removeElement($treballador);
 }

 public function getTreballador(){
  return $this->treballador;
 }

 public function addEmpresaxuser(\Agui\GestBundle\Entity\Empresaxusers $empresaxuser){
  $this->empresaxuser[] = $empresaxuser;
  return $this;
 }

 public function removeEmpresaxuser(\Agui\GestBundle\Entity\Empresaxusers $empresaxuser){
  $this->empresaxuser->removeElement($empresaxuser);
 }

 public function getEmpresaxuser(){
  return $this->empresaxuser;
 }

	public function __toString() {
		return $this->getNom();
	}

}