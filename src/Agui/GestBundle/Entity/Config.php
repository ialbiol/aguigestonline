<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Config {
	protected $versio;
	protected $password;
 protected $color;
 protected $tempsmin;
 protected $ruta1;
 protected $ruta2;
 protected $tiporet;
 protected $ivadef;
 protected $phoresdef;
 protected $rutamysql;
 protected $dieslimpres;
 protected $diesvenfact;

 protected $id;
 
 public function __construct() {

 }

 public function setVersio($versio){
  $this->versio = $versio;

  return $this;
 }

 public function getVersio(){
  return $this->versio;
 }

 public function setPassword($password){
  $this->password = $password;

  return $this;
 }

 public function getPassword(){
  return $this->password;
 }

 public function setColor($color){
  $this->color = $color;

  return $this;
 }

 public function getColor(){
  return $this->color;
 }

 public function setTempsmin($tempsmin){
  $this->tempsmin = $tempsmin;

  return $this;
 }

 public function getTempsmin(){
  return $this->tempsmin;
 }

 public function setRuta1($ruta1){
  $this->ruta1 = $ruta1;

  return $this;
 }

 public function getRuta1(){
  return $this->ruta1;
 }

 public function setRuta2($ruta2){
  $this->ruta2 = $ruta2;

  return $this;
 }

 public function getRuta2(){
  return $this->ruta2;
 }

 public function setTiporet($tiporet){
  $this->tiporet = $tiporet;

  return $this;
 }

 public function getTiporet(){
  return $this->tiporet;
 }

 public function setIvadef($ivadef){
  $this->ivadef = $ivadef;

  return $this;
 }

 public function getIvadef(){
  return $this->ivadef;
 }

 public function setPhoresdef($phoresdef){
  $this->phoresdef = $phoresdef;

  return $this;
 }

 public function getPhoresdef(){
  return $this->phoresdef;
 }

 public function setRutamysql($rutamysql){
  $this->rutamysql = $rutamysql;

  return $this;
 }

 public function getRutamysql(){
  return $this->rutamysql;
 }

 public function setDieslimpres($dieslimpres){
  $this->dieslimpres = $dieslimpres;

  return $this;
 }

 public function getDieslimpres(){
  return $this->dieslimpres;
 }

 public function setDiesvenfact($diesvenfact){
  $this->diesvenfact = $diesvenfact;

  return $this;
 }

 public function getDiesvenfact(){
  return $this->diesvenfact;
 }


	public function getId() {
		return $this->id;
	}

	public function __toString() {
		return $this->getVersio();
	}

}