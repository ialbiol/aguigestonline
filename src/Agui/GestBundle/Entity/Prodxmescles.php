<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Prodxmescles {
 protected $percent;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $producte;
 protected $base;

 public function __construct() {

 }

 public function setPercent($percent){
  $this->percent = $percent;

  return $this;
 }

 public function getPercent(){
  return $this->percent;
 }


	public function getId() {
		return $this->id;
	}

 public function setProducte(\Agui\GestBundle\Entity\Productes $producte = null) {
  $this->producte = $producte;
  return $this;
 }

 public function getProducte() {
  return $this->producte;
 }

 public function setBase(\Agui\GestBundle\Entity\Productes $base = null) {
  $this->base = $base;
  return $this;
 }

 public function getBase() {
  return $this->base;
 }

	public function __toString() {
		return $this->getPercent();
	}

//////////////////////////////////////////////////////////

	protected $num = null;

	public function setPreualbaran($preualbaran){
		return $this;
	}

	public function getPreualbaran(){
		$preu = 0;
		$prod = $this->getProducte();
		if($prod != null){
			$preu = $prod->getPalbaran() / $prod->getUnitats();
		}

		return $preu;
	}

	public function setNum($num){
		$this->num = $num;

		return $this;
	}

	public function getNum(){
		if($this->num == null){
			$prod = $this->getProducte();
			if($prod != null){
				$prod->GestioMescla();
			}
		}

		return $this->num;
	}

	public function setPreupercent($preupercent){
		return $this;
	}

	public function getPreupercent(){
		$preu = $this->getPreualbaran();
		$preu = $preu * ($this->getPercent() / 100);

		return $preu;
	}

}