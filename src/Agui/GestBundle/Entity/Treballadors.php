<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Treballadors {
	protected $nom;
	protected $direccio;
 protected $telf;
 protected $nif;
 protected $cc;
 protected $notes;
 protected $segsocial;
 protected $actiu;
 protected $estat; //camp pendent borrar
 protected $foto;
 protected $color;

 protected $id;

 // @var \Agui\GestBundle\Entity\Proveedors
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $factura;
 private $parte;
	private $fototreballador;

 public function __construct() {
  $this->factura = new \Doctrine\Common\Collections\ArrayCollection();
  $this->parte = new \Doctrine\Common\Collections\ArrayCollection();
		$this->fototreballador = new \Doctrine\Common\Collections\ArrayCollection();
  $this->estat = 0; //sempre. eliminar
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setDireccio($direccio){
  $this->direccio = $direccio;

  return $this;
 }

 public function getDireccio(){
  return $this->direccio;
 }

 public function setTelf($telf){
  $this->telf = $telf;

  return $this;
 }

 public function getTelf(){
  return $this->telf;
 }

 public function setNif($nif){
  $this->nif = $nif;

  return $this;
 }

 public function getNif(){
  return $this->nif;
 }

 public function setCc($cc){
  $this->cc = $cc;

  return $this;
 }

 public function getCc(){
  return $this->cc;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setSegsocial($segsocial){
  $this->segsocial = $segsocial;

  return $this;
 }

 public function getSegsocial(){
  return $this->segsocial;
 }

 public function setActiu($actiu){
  $this->actiu = $actiu;

  return $this;
 }

 public function getActiu(){
  return $this->actiu;
 }

 public function setEstat($estat){
  $this->estat = $estat;

  return $this;
 }

 public function getEstat(){
  return $this->estat;
 }

 public function setFoto($foto){
  $this->foto = $foto;

  return $this;
 }

 public function getFoto(){
  return $this->foto;
 }

 public function setColor($color){
  $this->color = $color;

  return $this;
 }

 public function getColor(){
  return $this->color;
 }


	public function getId() {
		return $this->id;
	}

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura[] = $factura;
  return $this;
 }

 public function removeFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura->removeElement($factura);
 }

 public function getFactura(){
  return $this->factura;
 }

 public function addParte(\Agui\GestBundle\Entity\Partes $parte){
  $this->parte[] = $parte;
  return $this;
 }

 public function removeParte(\Agui\GestBundle\Entity\Partes $parte){
  $this->parte->removeElement($parte);
 }

 public function getParte(){
  return $this->parte;
 }

 public function addFototreballador(\Agui\GestBundle\Entity\Fotos $fototreballador){
  $this->fototreballador[] = $fototreballador;
  return $this;
 }

 public function removeFototreballador(\Agui\GestBundle\Entity\Fotos $fototreballador){
  $this->fototreballador->removeElement($fototreballador);
 }

 public function getFototreballador(){
  return $this->fototreballador;
 }

	public function __toString() {
		return $this->getNom();
	}

}