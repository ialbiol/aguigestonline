<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Prodxfacts {
 protected $quantitatprepres;
 protected $quantitatpres;
 protected $quantitattreb;
 protected $quantitatfact;
 protected $cost;
 protected $nom = " ";
 protected $pcompra;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $producte;
 protected $factura;
 protected $tipoprod;

 // @var \Doctrine\Common\Collections\Collection
 private $prodxparte;

 public function __construct() {
  $this->prodxparte = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setQuantitatprepres($quantitatprepres){
		$prod = $this->getProducte();
		if($prod == null){ $this->quantitatprepres = $quantitatprepres; }
  else { $this->quantitatprepres = $prod->GuardarQuantitat($quantitatprepres); }

  return $this;
 }

 public function getQuantitatprepres(){
		$prod = $this->getProducte();
		if($prod == null){ return $this->quantitatprepres; }
  return $prod->MostrarQuantitat($this->quantitatprepres);
 }

 public function getNQuantitatprepres(){
  return $this->quantitatprepres;
 }

 public function setQuantitatpres($quantitatpres){
		$prod = $this->getProducte();
		if($prod == null){ $this->quantitatpres = $quantitatpres; }
  else { $this->quantitatpres = $prod->GuardarQuantitat($quantitatpres); }

  return $this;
 }

 public function getQuantitatpres(){
		$prod = $this->getProducte();
		if($prod == null){ return $this->quantitatpres; }
  return $prod->MostrarQuantitat($this->quantitatpres);
 }

 public function getNQuantitatpres(){
  return $this->quantitatpres;
 }

 public function setQuantitattreb($quantitattreb){
		$prod = $this->getProducte();
		if($prod == null){ $this->quantitattreb = $quantitattreb; }
  else { $this->quantitattreb = $prod->GuardarQuantitat($quantitattreb); }

  return $this;
 }

 public function getQuantitattreb(){
		$prod = $this->getProducte();
		if($prod == null){ return $this->quantitattreb; }
  return $prod->MostrarQuantitat($this->quantitattreb);
 }

 public function getNQuantitattreb(){
  return $this->quantitattreb;
 }

 public function setQuantitatfact($quantitatfact){
		$prod = $this->getProducte();
		if($prod == null){ $this->quantitatfact = $quantitatfact; }
  else { $this->quantitatfact = $prod->GuardarQuantitat($quantitatfact); }

  return $this;
 }

 public function getQuantitatfact(){
		$prod = $this->getProducte();
		if($prod == null){ return $this->quantitatfact; }
  return $prod->MostrarQuantitat($this->quantitatfact);
 }

 public function getNQuantitatfact(){
  return $this->quantitatfact;
 }

 public function setCost($cost){
		if($cost != ''){
			$this->cost = $cost;
		}

  return $this;
 }

 public function getCost(){
  return $this->cost;
 }

 public function setNom($nom){
  $this->nom = $nom;

		if($nom == null){	$this->nom = " ";	}

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setPcompra($pcompra){
  $this->pcompra = $pcompra;

  return $this;
 }

 public function getPcompra(){
  return $this->pcompra;
 }


	public function getId() {
		return $this->id;
	}

 public function setProducte(\Agui\GestBundle\Entity\Productes $producte = null) {
  $this->producte = $producte;

  if($producte != null){
   $this->setTipoprod($this->producte->getTipoprod());
   $this->setPcompra($this->producte->getPcompra());
   $this->setCost($this->producte->getPfactura());
  }

  return $this;
 }

 public function getProducte() {
  return $this->producte;
 }

 public function setFactura(\Agui\GestBundle\Entity\Factures $factura = null) {
  $this->factura = $factura;
  return $this;
 }

 public function getFactura() {
  return $this->factura;
 }

 public function setTipoprod(\Agui\GestBundle\Entity\Tipoprods $tipoprod = null) {
  $this->tipoprod = $tipoprod;
  return $this;
 }

 public function getTipoprod() {
  return $this->tipoprod;
 }

 public function addProdxparte(\Agui\GestBundle\Entity\Prodxpartes $prodxparte){
  $this->prodxparte[] = $prodxparte;
  return $this;
 }

 public function removeProdxparte(\Agui\GestBundle\Entity\Prodxpartes $prodxparte){
  $this->prodxparte->removeElement($prodxparte);
 }

 public function getProdxparte(){
  return $this->prodxparte;
 }

	public function __toString() {
		return $this->getNom();
	}

////////////////////////////////////////////////////

	public function setPercent($percent){
		return $this;
	}

	public function getPercent(){
		$prod = $this->getProducte();
		if($prod != null){
			return $prod->getPercent();
		}

		return 0;
	}

 public function setTotalpressupost($total){
  return $this;
 }

 public function getTotalpressupost(){
  return $this->quantitatpres * $this->cost;
 }

 public function setBeneficipressupost($total){
  return $this;
 }

 public function getBeneficipressupost(){
  return $this->quantitatpres * ($this->cost - $this->pcompra);
 }

 public function setTotaltreball($total){
  return $this;
 }

 public function getTotaltreball(){
  return $this->quantitattreb * $this->cost;
 }

 public function setBeneficitreball($total){
  return $this;
 }

 public function getBeneficitreball(){
  return $this->quantitattreb * ($this->cost - $this->pcompra);
 }

 public function setTotalfactura($total){
  return $this;
 }

 public function getTotalfactura(){
  return $this->quantitatfact * $this->cost;
 }

 public function setBeneficifactura($total){
  return $this;
 }

 public function getBeneficifactura(){
  return $this->quantitatfact * ($this->cost - $this->pcompra);
 }

}