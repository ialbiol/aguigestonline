<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Plantilles {
	protected $nom;
	protected $notes;
 protected $rutaimg1;
 protected $rutaimg2;
 protected $tipoun;
 protected $idtipoplan;
 protected $idtpclient;
 protected $idtptreb;
 protected $idtpttreb;
 protected $idtpacabat;
 protected $idtpcacabat;
 protected $idtoca;
 protected $notes2;

 protected $id;

 // @var \Doctrine\Common\Collections\Collection
 private $plantxclient;
 private $prodxplant;
	private $fotoplantilla;

 // @var \Agui\GestBundle\Entity\Clients
 protected $empresa;

 public function __construct() {
  $this->plantxclient = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxplant = new \Doctrine\Common\Collections\ArrayCollection();
		$this->fotoplantilla = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setRutaimg1($rutaimg1){
  $this->rutaimg1 = $rutaimg1;

  return $this;
 }

 public function getRutaimg1(){
  return $this->rutaimg1;
 }

 public function setRutaimg2($rutaimg2){
  $this->rutaimg2 = $rutaimg2;

  return $this;
 }

 public function getRutaimg2(){
  return $this->rutaimg2;
 }

 public function setTipoun($tipoun){
  $this->tipoun = $tipoun;

  return $this;
 }

 public function getTipoun(){
  return $this->tipoun;
 }

 public function setIdtipoplan($idtipoplan){
  $this->idtipoplan = $idtipoplan;

  return $this;
 }

 public function getIdtipoplan(){
  return $this->idtipoplan;
 }

 public function setIdtpclient($idtpclient){
  $this->idtpclient = $idtpclient;

  return $this;
 }

 public function getIdtpclient(){
  return $this->idtpclient;
 }

 public function setIdtptreb($idtptreb){
  $this->idtptreb = $idtptreb;

  return $this;
 }

 public function getIdtptreb(){
  return $this->idtptreb;
 }

 public function setIdtpttreb($idtpttreb){
  $this->idtpttreb = $idtpttreb;

  return $this;
 }

 public function getIdtpttreb(){
  return $this->idtpttreb;
 }

 public function setIdtpacabat($idtpacabat){
  $this->idtpacabat = $idtpacabat;

  return $this;
 }

 public function getIdtpacabat(){
  return $this->idtpacabat;
 }

 public function setIdtpcacabat($idtpcacabat){
  $this->idtpcacabat = $idtpcacabat;

  return $this;
 }

 public function getIdtpcacabat(){
  return $this->idtpcacabat;
 }

 public function setIdtoca($idtoca){
  $this->idtoca = $idtoca;

  return $this;
 }

 public function getIdtoca(){
  return $this->idtoca;
 }

 public function setNotes2($notes2){
  $this->notes2 = $notes2;

  return $this;
 }

 public function getNotes2(){
  return $this->notes2;
 }


	public function getId() {
		return $this->id;
	}

 public function addPlantxclient(\Agui\GestBundle\Entity\Plantxclients $plantxclient){
  $this->plantxclient[] = $plantxclient;
  return $this;
 }

 public function removePlantxclient(\Agui\GestBundle\Entity\Plantxclients $plantxclient){
  $this->plantxclient->removeElement($plantxclient);
 }

 public function getPlantxclient(){
  return $this->plantxclient;
 }

 public function addProdxplant(\Agui\GestBundle\Entity\Prodxplants $prodxplant){
  $this->prodxplant[] = $prodxplant;
  return $this;
 }

 public function removeProdxplant(\Agui\GestBundle\Entity\Prodxplants $prodxplant){
  $this->prodxplant->removeElement($prodxplant);
 }

 public function getProdxplant(){
  return $this->prodxplant;
 }

 public function addFotoplantilla(\Agui\GestBundle\Entity\Fotos $fotoplantilla){
  $this->fotoplantilla[] = $fotoplantilla;
  return $this;
 }

 public function removeFotoplantilla(\Agui\GestBundle\Entity\Fotos $fotoplantilla){
  $this->fotoplantilla->removeElement($fotoplantilla);
 }

 public function getFotoplantilla(){
  return $this->fotoplantilla;
 }

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

	public function __toString() {
		return $this->getNom();
	}

}