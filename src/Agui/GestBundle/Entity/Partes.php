<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Partes {
	protected $data;
 protected $hinici;
 protected $hfinal;
 protected $opcions;
 protected $notes;
 protected $descripcio;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $factura;
 protected $treballador;
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $prodxparte;

 public function __construct() {
  $this->prodxparte = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setData($data){
  $this->data = $data;

  return $this;
 }

 public function getData(){
  return $this->data;
 }

 public function setHinici($hinici){
  $this->hinici = $hinici;

  return $this;
 }

 public function getHinici(){
  return $this->hinici;
 }

 public function setHfinal($hfinal){
  $this->hfinal = $hfinal;

  return $this;
 }

 public function getHfinal(){
  return $this->hfinal;
 }

 public function setOpcions($opcions){
  $this->opcions = $opcions;

  return $this;
 }

 public function getOpcions(){
  return $this->opcions;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setDescripcio($descripcio){
  $this->descripcio = $descripcio;

  return $this;
 }

 public function getDescripcio(){
  return $this->descripcio;
 }


	public function getId() {
		return $this->id;
	}

 public function setFactura(\Agui\GestBundle\Entity\Factures $factura = null) {
  $this->factura = $factura;
  return $this;
 }

 public function getFactura() {
  return $this->factura;
 }

 public function setTreballador(\Agui\GestBundle\Entity\Treballadors $treballador = null) {
  $this->treballador = $treballador;
  return $this;
 }

 public function getTreballador() {
  return $this->treballador;
 }

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addProdxparte(\Agui\GestBundle\Entity\Prodxpartes $prodxparte){
  $this->prodxparte[] = $prodxparte;
  return $this;
 }

 public function removeProdxparte(\Agui\GestBundle\Entity\Prodxpartes $prodxparte){
  $this->prodxparte->removeElement($prodxparte);
 }

 public function getProdxparte(){
  return $this->prodxparte;
 }

	public function __toString() {
		return $this->getDescripcio();
	}

}