<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Pagaments {
	protected $data;
 protected $quantitat;
 protected $tpagament;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $factura;
 protected $compra;

 public function __construct() {

 }

 public function setData($data){
  $this->data = $data;

  return $this;
 }

 public function getData(){
  return $this->data;
 }

 public function setQuantitat($quantitat){
  $this->quantitat = $quantitat;

  return $this;
 }

 public function getQuantitat(){
  return $this->quantitat;
 }

 public function setTpagament($tpagament){
  $this->tpagament = $tpagament;

  return $this;
 }

 public function getTpagament(){
  return $this->tpagament;
 }


	public function getId() {
		return $this->id;
	}

 public function setFactura(\Agui\GestBundle\Entity\Factures $factura = null) {
  $this->factura = $factura;
  return $this;
 }

 public function getFactura() {
  return $this->factura;
 }

 public function setCompra(\Agui\GestBundle\Entity\Compres $compra = null) {
  $this->compra = $compra;
  return $this;
 }

 public function getCompra() {
  return $this->compra;
 }

	public function __toString() {
		return $this->getTpagament();
	}

}