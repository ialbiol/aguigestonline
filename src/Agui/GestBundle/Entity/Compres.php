<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Compres {
	protected $idllibre;
 protected $tconta;
 protected $estat;
 protected $numfact;
 protected $data;
 protected $datapagada;
 protected $tipoiva;
 protected $sum;
 protected $total;
 protected $iva;
 protected $notes;
 protected $descripcio;
 protected $naturalesa;
 protected $tipodte;
 protected $dte;
 protected $pendent;
 protected $acompte;

 protected $id;

 // @var \Agui\GestBundle\Entity\Proveedors
 protected $proveedor;
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $pagament;
 private $prodxcompra;

 public function __construct() {
  $this->pagament = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxcompra = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setIdllibre($idllibre){
  $this->idllibre = $idllibre;

  return $this;
 }

 public function getIdllibre(){
  return $this->idllibre;
 }

 public function setTconta($tconta){
  $this->tconta = $tconta;

  return $this;
 }

 public function getTconta(){
  return $this->tconta;
 }

 public function setEstat($estat){
  $this->estat = $estat;

		$this->ControlEstats();

  return $this;
 }

 public function getEstat(){
  return $this->estat;
 }

 public function setNumfact($numfact){
  $this->numfact = $numfact;

  return $this;
 }

 public function getNumfact(){
  return $this->numfact;
 }

 public function setData($data){
  $this->data = $data;

  return $this;
 }

 public function getData(){
  return $this->data;
 }

 public function setDatapagada($datapagada){
  $this->datapagada = $datapagada;

  return $this;
 }

 public function getDatapagada(){
  return $this->datapagada;
 }

 public function setTipoiva($tipoiva){
  $this->tipoiva = $tipoiva;

  return $this;
 }

 public function getTipoiva(){
  return $this->tipoiva;
 }

 public function setSum($sum){
  $this->sum = $sum;

  return $this;
 }

 public function getSum(){
  return $this->sum;
 }

 public function setTotal($total){
  $this->total = $total;

  return $this;
 }

 public function getTotal(){
  return $this->total;
 }

 public function setIva($iva){
  $this->iva = $iva;

  return $this;
 }

 public function getIva(){
  return $this->iva;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setDescripcio($descripcio){
  $this->descripcio = $descripcio;

  return $this;
 }

 public function getDescripcio(){
  return $this->descripcio;
 }

 public function setNaturalesa($naturalesa){
  $this->naturalesa = $naturalesa;

  return $this;
 }

 public function getNaturalesa(){
  return $this->naturalesa;
 }

 public function setTipodte($tipodte){
  $this->tipodte = $tipodte;

  return $this;
 }

 public function getTipodte(){
  return $this->tipodte;
 }

 public function setDte($dte){
  $this->dte = $dte;

  return $this;
 }

 public function getDte(){
  return $this->dte;
 }

 public function setPendent($pendent){
  $this->pendent = $pendent;

  return $this;
 }

 public function getPendent(){
  return $this->pendent;
 }

 public function setAcompte($acompte){
  $this->acompte = $acompte;

  return $this;
 }

 public function getAcompte(){
  return $this->acompte;
 }


	public function getId() {
		return $this->id;
	}

 public function setProveedor(\Agui\GestBundle\Entity\Proveedors $proveedor = null) {
  $this->proveedor = $proveedor;
  return $this;
 }

 public function getProveedor() {
  return $this->proveedor;
 }

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addPagament(\Agui\GestBundle\Entity\Pagaments $pagament){
  $this->pagament[] = $pagament;
  return $this;
 }

 public function removePagament(\Agui\GestBundle\Entity\Pagaments $pagament){
  $this->pagament->removeElement($pagament);
 }

 public function getPagament(){
  return $this->pagament;
 }

 public function addProdxcompra(\Agui\GestBundle\Entity\Prodxcompres $prodxcompra){
  $this->prodxcompra[] = $prodxcompra;
  return $this;
 }

 public function removeProdxcompra(\Agui\GestBundle\Entity\Prodxcompres $prodxcompra){
  $this->prodxcompra->removeElement($prodxcompra);
 }

 public function getProdxcompra(){
  return $this->prodxcompra;
 }

	public function __toString() {
		return $this->getDescripcio();
	}

//-----------------------------

//funcio que actualitza els cams dels totals
	public function CalculTotals(){
		$prods = $this->getProdxcompra();
		$pagos = $this->getPagament();

		//sumo totals
		$base = 0;
		foreach($prods as $prod){
			$t = $prod->getQuantitat() * $prod->getCost();
			$t = $t - ($t * ($prod->getDte() / 100)); //dte per producte
			$base = $base + $t;
		}
		$dte = $base * ($this->getTipodte() / 100); //dte global factura
		$total = $base - $dte;
		$iva = $total * ($this->getTipoiva() / 100); //iva factura
		$total = $total + $iva;

		//sumo pagaments
		$totalPagat = 0;
		foreach($pagos as $pago){
			$totalPagat = $totalPagat + $pago->getQuantitat();
		}
		$pendent = $total - $totalPagat;

		//actualitzo camps
		$this->setSum($base);
		$this->setIva($iva);
		$this->setDte($dte);
		$this->setTotal($total);
		
		$this->setPendent($pendent);
		$this->setAcompte($totalPagat);
	}

//funcio que controla els canvis d'estat
	public function ControlEstats(){
		if($this->estat == 10){ //pagada
			if($this->getDatapagada() == null){ //si no tinc data pagada
				$this->setDatapagada(new \DateTime()); //poso data
			}
		}
	}

}