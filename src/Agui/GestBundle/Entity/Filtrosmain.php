<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Filtrosmain
 */
class Filtrosmain {

 protected $empresa = "";
 protected $any = "";
 protected $client = "%";

 public function __construct() {
 }

 public function setEmpresa($empresa){
  $this->empresa = $empresa;

  return $this;
 }

 public function getEmpresa(){
  return $this->empresa;
 }

 public function setAny($any){
  $this->any = $any;

  return $this;
 }

 public function getAny(){
  return $this->any;
 }

 public function setClient($client){
  $this->client = $client;

  return $this;
 }

 public function getClient(){
  return $this->client;
 }

}