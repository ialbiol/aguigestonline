<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Planings {
	protected $dia;
	protected $hora;
 protected $duracio;
 protected $nota;
 protected $estat;

 protected $id;
 
 public function __construct() {

 }

 public function setDia($dia){
  $this->dia = $dia;

  return $this;
 }

 public function getDia(){
  return $this->dia;
 }

 public function setHora($hora){
  $this->hora = $hora;

  return $this;
 }

 public function getHora(){
  return $this->hora;
 }

 public function setDuracio($duracio){
  $this->duracio = $duracio;

  return $this;
 }

 public function getDuracio(){
  return $this->duracio;
 }

 public function setNota($nota){
  $this->nota = $nota;

  return $this;
 }

 public function getNota(){
  return $this->nota;
 }

 public function setEstat($estat){
  $this->estat = $estat;

  return $this;
 }

 public function getEstat(){
  return $this->estat;
 }


	public function getId() {
		return $this->id;
	}

	public function __toString() {
		return $this->getNota();
	}

}