<?php
namespace Agui\GestBundle\Entity;

use Agui\GestBundle\Entity\Auxselects;
use Doctrine\ORM\Mapping as ORM;

class Factures {
	protected $descripcio;
 protected $info;
 protected $estat;
 protected $refclient;
 protected $tipoclient;
 protected $tipoiva;
 protected $ptancat;
 protected $tconta;
 protected $datapres;
 protected $sumpres;
 protected $totalpres;
 protected $ivapres;
 protected $retenciopres;
 protected $sumtreb;
 protected $totaltreb;
 protected $ivatreb;
	protected $retenciotreb;
	protected $datafact;
 protected $sumfact;
 protected $totalfact;
 protected $ivafact;
 protected $retenciofact;
 protected $tnumfact;
 protected $numfact;
 protected $totaldeute;
 protected $acompte;
 protected $datapresentat;
 protected $dataaceptat;
 protected $dataentrega;
 protected $dataenproces;
 protected $dataacabat;
 protected $datafactpresentada;
 protected $datacobro;
 protected $notes;
	protected $sumadeute;
 protected $numpres;
 protected $tnumpres;
 protected $notesfact;
 protected $comisio;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $client;
 protected $clientfact;
 protected $treballador;
 protected $tipotreb;
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $fotofactura;
 private $pagament;
 private $parte;
 private $prodxfact;

 public function __construct() {
  $this->fotofactura = new \Doctrine\Common\Collections\ArrayCollection();
  $this->pagament = new \Doctrine\Common\Collections\ArrayCollection();
  $this->parte = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxfact = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setDescripcio($descripcio){
  $this->descripcio = $descripcio;

  return $this;
 }

 public function getDescripcio(){
  return $this->descripcio;
 }

 public function setInfo($info){
  $this->info = $info;

  return $this;
 }

 public function getInfo(){
  return $this->info;
 }

 public function setEstat($estat){
  $this->estat = $estat;

  $this->ControlEstats();

  return $this;
 }

 public function getEstat(){
  return $this->estat;
 }

 public function setRefclient($refclient){
  $this->refclient = $refclient;

  return $this;
 }

 public function getRefclient(){
  return $this->refclient;
 }

 public function setTipoclient($tipoclient){
  $this->tipoclient = $tipoclient;

  return $this;
 }

 public function getTipoclient(){
  return $this->tipoclient;
 }

 public function setTipoiva($tipoiva){
  $this->tipoiva = $tipoiva;

  return $this;
 }

 public function getTipoiva(){
  return $this->tipoiva;
 }

 public function setPtancat($ptancat){
  $this->ptancat = $ptancat;

  return $this;
 }

 public function getPtancat(){
  return $this->ptancat;
 }

 public function setTconta($tconta){
  $this->tconta = $tconta;

  return $this;
 }

 public function getTconta(){
  return $this->tconta;
 }

 public function setDatapres($datapres){
  $this->datapres = $datapres;

  return $this;
 }

 public function getDatapres(){
  return $this->datapres;
 }

 public function setSumpres($sumpres){
  $this->sumpres = $sumpres;

  return $this;
 }

 public function getSumpres(){
  return $this->sumpres;
 }

 public function setTotalpres($totalpres){
  $this->totalpres = $totalpres;

  return $this;
 }

 public function getTotalpres(){
  return $this->totalpres;
 }

 public function setIvapres($ivapres){
  $this->ivapres = $ivapres;

  return $this;
 }

 public function getIvapres(){
  return $this->ivapres;
 }

 public function setRetenciopres($retenciopres){
  $this->retenciopres = $retenciopres;

  return $this;
 }

 public function getRetenciopres(){
  return $this->retenciopres;
 }

 public function setSumtreb($sumtreb){
  $this->sumtreb = $sumtreb;

  return $this;
 }

 public function getSumtreb(){
  return $this->sumtreb;
 }

 public function setTotaltreb($totaltreb){
  $this->totaltreb = $totaltreb;

  return $this;
 }

 public function getTotaltreb(){
  return $this->totaltreb;
 }

 public function setIvatreb($ivatreb){
  $this->ivatreb = $ivatreb;

  return $this;
 }

 public function getIvatreb(){
  return $this->ivatreb;
 }

 public function setRetenciotreb($retenciotreb){
  $this->retenciotreb = $retenciotreb;

  return $this;
 }

 public function getRetenciotreb(){
  return $this->retenciotreb;
 }

 public function setDatafact($datafact){
  $this->datafact = $datafact;

  return $this;
 }

 public function getDatafact(){
  return $this->datafact;
 }

 public function setSumfact($sumfact){
  $this->sumfact = $sumfact;

  return $this;
 }

 public function getSumfact(){
  return $this->sumfact;
 }

 public function setTotalfact($totalfact){
  $this->totalfact = $totalfact;

  return $this;
 }

 public function getTotalfact(){
  return $this->totalfact;
 }

 public function setIvafact($ivafact){
  $this->ivafact = $ivafact;

  return $this;
 }

 public function getIvafact(){
  return $this->ivafact;
 }

 public function setRetenciofact($retenciofact){
  $this->retenciofact = $retenciofact;

  return $this;
 }

 public function getRetenciofact(){
  return $this->retenciofact;
 }

 public function setTnumfact($tnumfact){
  $this->tnumfact = $tnumfact;

  return $this;
 }

 public function getTnumfact(){
  return $this->tnumfact;
 }

 public function setNumfact($numfact){
  $this->numfact = $numfact;

  return $this;
 }

 public function getNumfact(){
  return $this->numfact;
 }

 public function setTotaldeute($totaldeute){
  $this->totaldeute = $totaldeute;

  return $this;
 }

 public function getTotaldeute(){
  return $this->totaldeute;
 }

 public function setAcompte($acompte){
  $this->acompte = $acompte;

  return $this;
 }

 public function getAcompte(){
  return $this->acompte;
 }

 public function setDatapresentat($datapresentat){
  $this->datapresentat = $datapresentat;

  return $this;
 }

 public function getDatapresentat(){
  return $this->datapresentat;
 }

 public function setDataaceptat($dataaceptat){
  $this->dataaceptat = $dataaceptat;

  return $this;
 }

 public function getDataaceptat(){
  return $this->dataaceptat;
 }

 public function setDataentrega($dataentrega){
  $this->dataentrega = $dataentrega;

  return $this;
 }

 public function getDataentrega(){
  return $this->dataentrega;
 }

 public function setDataenproces($dataenproces){
  $this->dataenproces = $dataenproces;

  return $this;
 }

 public function getDataenproces(){
  return $this->dataenproces;
 }

 public function setDataacabat($dataacabat){
  $this->dataacabat = $dataacabat;

  return $this;
 }

 public function getDataacabat(){
  return $this->dataacabat;
 }

 public function setDatafactpresentada($datafactpresentada){
  $this->datafactpresentada = $datafactpresentada;

  return $this;
 }

 public function getDatafactpresentada(){
  return $this->datafactpresentada;
 }

 public function setDatacobro($datacobro){
  $this->datacobro = $datacobro;

  return $this;
 }

 public function getDatacobro(){
  return $this->datacobro;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setSumadeute($sumadeute){
  $this->sumadeute = $sumadeute;

  return $this;
 }

 public function getSumadeute(){
  return $this->sumadeute;
 }

 public function setNumpres($numpres){
  $this->numpres = $numpres;

  return $this;
 }

 public function getNumpres(){
  return $this->numpres;
 }

 public function setTnumpres($tnumpres){
  $this->tnumpres = $tnumpres;

  return $this;
 }

 public function getTnumpres(){
  return $this->tnumpres;
 }

 public function setNotesfact($notesfact){
  $this->notesfact = $notesfact;

  return $this;
 }

 public function getNotesfact(){
  return $this->notesfact;
 }

 public function setComisio($comisio){
  $this->comisio = $comisio;

  return $this;
 }

 public function getComisio(){
  return $this->comisio;
 }


	public function getId() {
		return $this->id;
	}

 public function setClient(\Agui\GestBundle\Entity\Clients $client = null) {
  $this->client = $client;
  return $this;
 }

 public function getClient() {
  return $this->client;
 }

 public function setClientfact(\Agui\GestBundle\Entity\Clients $clientfact = null) {
  $this->clientfact = $clientfact;
  return $this;
 }

 public function getClientfact() {
  return $this->clientfact;
 }

 public function setTreballador(\Agui\GestBundle\Entity\Treballadors $treballador = null) {
  $this->treballador = $treballador;
  return $this;
 }

 public function getTreballador() {
  return $this->treballador;
 }

 public function setTipotreb(\Agui\GestBundle\Entity\Tipotrebs $tipotreb = null) {
  $this->tipotreb = $tipotreb;
  return $this;
 }

 public function getTipotreb() {
  return $this->tipotreb;
 }

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addFotofactura(\Agui\GestBundle\Entity\Fotos $fotofactura){
  $this->fotofactura[] = $fotofactura;
  return $this;
 }

 public function removeFotofactura(\Agui\GestBundle\Entity\Fotos $fotofactura){
  $this->fotofactura->removeElement($fotofactura);
 }

 public function getFotofactura(){
  return $this->fotofactura;
 }

 public function addPagament(\Agui\GestBundle\Entity\Pagaments $pagament){
  $this->pagament[] = $pagament;
  return $this;
 }

 public function removePagament(\Agui\GestBundle\Entity\Pagaments $pagament){
  $this->pagament->removeElement($pagament);
 }

 public function getPagament(){
  return $this->pagament;
 }

 public function addParte(\Agui\GestBundle\Entity\Partes $parte){
  $this->parte[] = $parte;
  return $this;
 }

 public function removeParte(\Agui\GestBundle\Entity\Partes $parte){
  $this->parte->removeElement($parte);
 }

 public function getParte(){
  return $this->parte;
 }

 public function addProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact){
  $this->prodxfact[] = $prodxfact;
  return $this;
 }

 public function removeProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact){
  $this->prodxfact->removeElement($prodxfact);
 }

 public function getProdxfact(){
  return $this->prodxfact;
 }

	public function __toString() {
		return $this->getDescripcio() ? : 'n/a';
	}

/*******************************************/

	public function getEstatpressupost() {
		return Auxselects::FindEstatsPressupostos($this->estat);
	}

	public function getEstattreball() {
		return Auxselects::FindEstatsTreballs($this->estat);
	}

	public function getEstatfactura() {
		return Auxselects::FindEstatsFactures($this->estat);
	}

//funcio que actualitza els cams dels totals del pressupost
	public function CalculTotalsPressupost(){
		$prods = $this->getProdxfact();

		//sumo totals
		$base = 0;
		foreach($prods as $prod){
			$t = $prod->getQuantitatpres() * $prod->getCost();
			$base = $base + $t;
		}
		$total = $base;
		$iva = $total * ($this->getTipoiva() / 100); //iva factura
		$total = $total + $iva;

		//actualitzo camps
		$this->setSumpres($base);
		$this->setIvapres($iva);
		$this->setTotalpres($total);
  $this->setRetenciopres(0);
	}

//funcio que actualitza els cams dels totals
	public function CalculTotalsTreball(){
		$prods = $this->getProdxfact();

		//sumo totals
		$base = 0;
		foreach($prods as $prod){
			$t = $prod->getQuantitattreb() * $prod->getCost();
			$base = $base + $t;
		}
		$total = $base;
		$iva = $total * ($this->getTipoiva() / 100); //iva factura
		$total = $total + $iva;

		//actualitzo camps
		$this->setSumtreb($base);
		$this->setIvatreb($iva);
		$this->setTotaltreb($total);
  $this->setRetenciotreb(0);
	}

//funcio que actualitza els cams dels totals
	public function CalculTotalsFactura(){
		$prods = $this->getProdxfact();
		$pagos = $this->getPagament();

		//sumo totals
		$base = 0;
		foreach($prods as $prod){
			$t = $prod->getQuantitatfact() * $prod->getCost();
			$base = $base + $t;
		}
		$total = $base;
		$iva = $total * ($this->getTipoiva() / 100); //iva factura
		$total = $total + $iva;

		//sumo pagaments
		$totalPagat = 0;
		foreach($pagos as $pago){
			$totalPagat = $totalPagat + $pago->getQuantitat();
		}
		$pendent = $total - $totalPagat;

		//actualitzo camps
		$this->setSumfact($base);
		$this->setIvafact($iva);
		$this->setTotalfact($total);
  $this->setRetenciofact(0);

  $this->setTotaldeute($total);
		$this->setSumadeute($pendent);
		$this->setAcompte($totalPagat);
	}

//funcio que controla els canvis d'estat
	public function ControlEstats(){
/*		if($this->estat == 10){ //pagada
			if($this->getDatapagada() == null){ //si no tinc data pagada
				$this->setDatapagada(new \DateTime()); //poso data
			}
		}*/
	}

}