<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Tipoplans {
	protected $nom;
	protected $tipo;
 protected $idtoca;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $client;
 protected $empresa;

 public function __construct() {

 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setTipo($tipo){
  $this->tipo = $tipo;

  return $this;
 }

 public function getTipo(){
  return $this->tipo;
 }

 public function setIdtoca($idtoca){
  $this->idtoca = $idtoca;

  return $this;
 }

 public function getIdtoca(){
  return $this->idtoca;
 }


	public function getId() {
		return $this->id;
	}

 public function setClient(\Agui\GestBundle\Entity\Clients $client = null) {
  $this->client = $client;
  return $this;
 }

 public function getClient() {
  return $this->client;
 }

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

	public function __toString() {
		return $this->getNom();
	}

//////////////////////////////////

	public function getTipotext() {
		return Auxselects::FindPlantillesClasesTipo($this->tipo);
	}

	public function getModeltext() {
		return Auxselects::FindPlantillesModels($this->idtoca);
	}

}