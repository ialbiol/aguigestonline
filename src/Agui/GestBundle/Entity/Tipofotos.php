<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Tipofotos {
	protected $nom;
	protected $grup;
	protected $orden;

 protected $id;

 // @var \Doctrine\Common\Collections\Collection
 private $foto;

 public function __construct() {
  $this->foto = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setGrup($grup){
  $this->grup = $grup;

  return $this;
 }

 public function getGrup(){
  return $this->grup;
 }

 public function setOrden($orden){
  $this->orden = $orden;

  return $this;
 }

 public function getOrden(){
  return $this->orden;
 }


	public function getId() {
		return $this->id;
	}

 public function addFoto(\Agui\GestBundle\Entity\Fotos $foto){
  $this->foto[] = $foto;
  return $this;
 }

 public function removeFoto(\Agui\GestBundle\Entity\Fotos $foto){
  $this->foto->removeElement($foto);
 }

 public function getFoto(){
  return $this->foto;
 }

	public function __toString() {
		return $this->getNom();
	}

////////////////////////////////////////

	public function getGruptext() {
		return Auxselects::FindGrupFotos($this->grup);
	}

}