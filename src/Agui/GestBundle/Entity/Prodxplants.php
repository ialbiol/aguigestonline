<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Prodxplants {
 protected $quantitat;
 protected $qpre;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $producte;
 protected $plantilla;
 protected $tipoprod;

 public function __construct() {

 }

 public function setQuantitat($quantitat){
		$prod = $this->getProducte();
		if($prod == null){ $this->quantitat = $quantitat; }
  else{ $this->quantitat = $prod->GuardarQuantitat($quantitat); }

  return $this;
 }

 public function getQuantitat(){
		$prod = $this->getProducte();
		if($prod == null){ return $this->quantitat; }
  return $prod->MostrarQuantitat($this->quantitat);
 }

	public function getNQuantitat(){
		return $this->quantitat;
	}

 public function setQpre($qpre){
		$prod = $this->getProducte();
		if($prod == null){ $this->qpre = $qpre; }
  else{ $this->qpre = $prod->GuardarQuantitat($qpre); }

  return $this;
 }

 public function getQpre(){
		$prod = $this->getProducte();
		if($prod == null){ return $this->qpre; }
  return $prod->MostrarQuantitat($this->qpre);
 }

 public function getNQpre(){
		return $this->qpre;
	}

	public function getId() {
		return $this->id;
	}

 public function setProducte(\Agui\GestBundle\Entity\Productes $producte = null) {
  $this->producte = $producte;

		$this->setTipoprod($this->producte->getTipoprod());

  return $this;
 }

 public function getProducte() {
  return $this->producte;
 }

 public function setPlantilla(\Agui\GestBundle\Entity\Plantilles $plantilla = null) {
  $this->plantilla = $plantilla;
  return $this;
 }

 public function getPlantilla() {
  return $this->plantilla;
 }

 public function setTipoprod(\Agui\GestBundle\Entity\Tipoprods $tipoprod = null) {
  $this->tipoprod = $tipoprod;
  return $this;
 }

 public function getTipoprod() {
  return $this->tipoprod;
 }

	public function __toString() {
		return $this->getQuantitat();
	}

///////////////////////////////////////////////////////////

	public function setPercent($percent){
		return $this;
	}

	public function getPercent(){
		$prod = $this->getProducte();
		if($prod != null){
			return $prod->getPercent();
		}

		return 0;
	}

}