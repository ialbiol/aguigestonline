<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Empresaxusers {
 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $empresa;
 protected $user;


 public function __construct() {
 }


	public function getId() {
		return $this->id;
	}

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null) {
  $this->user = $user;
  return $this;
 }

 public function getUser() {
  return $this->user;
 }

	public function __toString() {
		return $this->getId();
	}

}