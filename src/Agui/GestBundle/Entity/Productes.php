<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Productes {
	protected $nom;
	protected $refcolor;
 protected $acabats;
 protected $idllibre = 0; //tots els nous productes tenen idllibre 0
 protected $qalmacen;
 protected $qminima;
 protected $pfactura;
 protected $pcompra;
 protected $percent;
 protected $notes;
 protected $idfiltro;
 protected $unitats;
 protected $palbaran;
 protected $notesp;
 protected $marca;
	protected $imatge;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $tipoprod;
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $prodxcompra;
 private $prodxfact;
 private $prodxplant;
 private $prodxmescla;
 private $prodbase;
 private $prodcompost;
 private $prodcompostpart;
	private $fotoproducte;

 public function __construct() {
  $this->prodxcompra = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxfact = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxplant = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxmescla = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodbase = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodcompost = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodcompostpart = new \Doctrine\Common\Collections\ArrayCollection();
		$this->fotoproducte = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setRefcolor($refcolor){
  $this->refcolor = $refcolor;

  return $this;
 }

 public function getRefcolor(){
  return $this->refcolor;
 }

 public function setAcabats($acabats){
  $this->acabats = $acabats;

  return $this;
 }

 public function getAcabats(){
  return $this->acabats;
 }

 public function setIdllibre($idllibre){
  $this->idllibre = $idllibre;

  return $this;
 }

 public function getIdllibre(){
  return $this->idllibre;
 }

 public function setQalmacen($qalmacen){
  $this->qalmacen = $qalmacen;
		if($this->qalmacen == ""){ $this->qalmacen = 0; }

  return $this;
 }

 public function getQalmacen(){
  return $this->qalmacen;
 }

 public function setQminima($qminima){
  $this->qminima = $qminima;
		if($this->qminima == ""){ $this->qminima = 0; }

  return $this;
 }

 public function getQminima(){
  return $this->qminima;
 }

 public function setPfactura($pfactura){
  $this->pfactura = $pfactura;
		if($this->pfactura == ""){ $this->pfactura = 0; }

  return $this;
 }

 public function getPfactura(){
  return $this->pfactura;
 }

 public function setPcompra($pcompra){
  $this->pcompra = $pcompra;
		if($this->pcompra == ""){ $this->pcompra = 0; }

  return $this;
 }

 public function getPcompra(){
  return $this->pcompra;
 }

 public function setPercent($percent){
  $this->percent = $percent;
		if($this->percent == ""){ $this->percent = 0; }

  return $this;
 }

 public function getPercent(){
  return $this->percent;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setIdfiltro($idfiltro){
  $this->idfiltro = $idfiltro;

  return $this;
 }

 public function getIdfiltro(){
  return $this->idfiltro;
 }

 public function setUnitats($unitats){
  $this->unitats = $unitats;
		if($this->unitats == ""){ $this->unitats = 1; }

  return $this;
 }

 public function getUnitats(){
		if(($this->unitats == "") or ($this->unitats == 0)){ $this->unitats = 1; }
  return $this->unitats;
 }

 public function setPalbaran($palbaran){
  $this->palbaran = $palbaran;
		if($this->palbaran == ""){ $this->palbaran = 0; }

  return $this;
 }

 public function getPalbaran(){
		if($this->palbaran == ""){ $this->palbaran = 0; }
  return $this->palbaran;
 }

 public function setNotesp($notesp){
  $this->notesp = $notesp;

  return $this;
 }

 public function getNotesp(){
  return $this->notesp;
 }

 public function setMarca($marca){
  $this->marca = $marca;

  return $this;
 }

 public function getMarca(){
  return $this->marca;
 }

 public function setImatge($imatge){
  $this->imatge = $imatge;

  return $this;
 }

 public function getImatge(){
  return $this->imatge;
 }


	public function getId() {
		return $this->id;
	}

 public function setTipoprod(\Agui\GestBundle\Entity\Tipoprods $tipoprod = null) {
  $this->tipoprod = $tipoprod;
  return $this;
 }

 public function getTipoprod() {
  return $this->tipoprod;
 }

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addProdxcompra(\Agui\GestBundle\Entity\Prodxcompres $prodxcompra){
  $this->prodxcompra[] = $prodxcompra;
  return $this;
 }

 public function removeProdxcompra(\Agui\GestBundle\Entity\Prodxcompres $prodxcompra){
  $this->prodxcompra->removeElement($prodxcompra);
 }

 public function getProdxcompra(){
  return $this->prodxcompra;
 }

 public function addProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact){
  $this->prodxfact[] = $prodxfact;
  return $this;
 }

 public function removeProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact){
  $this->prodxfact->removeElement($prodxfact);
 }

 public function getProdxfact(){
  return $this->prodxfact;
 }

 public function addProdxplant(\Agui\GestBundle\Entity\Prodxplants $prodxplant){
  $this->prodxplant[] = $prodxplant;
  return $this;
 }

 public function removeProdxplant(\Agui\GestBundle\Entity\Prodxplants $prodxplant){
  $this->prodxplant->removeElement($prodxplant);
 }

 public function getProdxplant(){
  return $this->prodxplant;
 }

 public function addProdxmescla(\Agui\GestBundle\Entity\Prodxmescles $prodxmescla){
  $this->prodxmescla[] = $prodxmescla;
  return $this;
 }

 public function removeProdxmescla(\Agui\GestBundle\Entity\Prodxmescles $prodxmescla){
  $this->prodxmescla->removeElement($prodxmescla);
 }

 public function getProdxmescla(){
  return $this->prodxmescla;
 }

 public function addProdbase(\Agui\GestBundle\Entity\Prodxmescles $prodbase){
  $this->prodbase[] = $prodbase;
  return $this;
 }

 public function removeProdbase(\Agui\GestBundle\Entity\Prodxmescles $prodbase){
  $this->prodbase->removeElement($prodbase);
 }

 public function getProdbase(){
  return $this->prodbase;
 }

 public function addProdcompost(\Agui\GestBundle\Entity\Prodcomposts $prodcompost){
  $this->prodcompost[] = $prodcompost;
  return $this;
 }

 public function removeProdcompost(\Agui\GestBundle\Entity\Prodcomposts $prodcompost){
  $this->prodcompost->removeElement($prodcompost);
 }

 public function getProdcompost(){
  return $this->prodcompost;
 }

 public function addProdcompostpart(\Agui\GestBundle\Entity\Prodcomposts $prodcompostpart){
  $this->prodcompostpart[] = $prodcompostpart;
  return $this;
 }

 public function removeProdcompostpart(\Agui\GestBundle\Entity\Prodcomposts $prodcompostpart){
  $this->prodcompostpart->removeElement($prodcompostpart);
 }

 public function getProdcompostpart(){
  return $this->prodcompostpart;
 }

 public function addFotoproducte(\Agui\GestBundle\Entity\Fotos $fotoproducte){
  $this->fotoproducte[] = $fotoproducte;
  return $this;
 }

 public function removeFotoproducte(\Agui\GestBundle\Entity\Fotos $fotoproducte){
  $this->fotoproducte->removeElement($fotoproducte);
 }

 public function getFotoproducte(){
  return $this->fotoproducte;
 }

	public function __toString() {
		return $this->getNom();
	}

/////////////////////////////////////////////

	public function getBeneficiperunitat(){
		return $this->getPfactura() - $this->getPcompra();
	}

	public function getIdfiltrotext() {
		return Auxselects::FindFiltroProducte($this->idfiltro);
	}

	//converteix de 1,5 a 1:30 si fa falta
	public function MostrarQuantitat($quantitat){
		if($quantitat == ""){ return 0; }

		if($this->getTipoprod()->getId() == 1){ //hores
			if(strpos($quantitat, ',') !== false){ $num = explode(',', $quantitat); }
			else{ $num = explode('.', $quantitat); }

			if(count($num) == 1){ return $quantitat . ':00'; }
			else{
				$num[1] = '0.' . $num[1];

				if(($num[1] * 60) > 9){ return $num[0] . ':' . number_format(($num[1] * 60), 0); }
				else{ return $num[0] . ':0' . number_format(($num[1] * 60), 0); }
			}
		} else { return $quantitat; } //si no son hores, no faig res
	}

	//converteix de 1:30 a 1,5 si fa falta
	public function GuardarQuantitat($quantitat){
		if($quantitat == ""){ return 0; }
		$quantitat = str_replace(',', '.', $quantitat);

		if($this->getTipoprod()->getId() == 1){ //hores
			$num = explode(':', $quantitat);

			if(count($num) == 1){ return $quantitat; }
			else{
				if($num[1] == '00'){ return $quantitat; }
				else{ return $num[0] + ($num[1] / 60); }
			}
		} else { return $quantitat; } //si no son hores, no faig res
	}


	/*********** mescles *********************/
	public function GestioMescla(){
		$prodsmescla = $this->getProdbase();

		//calculo els nums de la mescla
		$min = 100;
		foreach($prodsmescla as $prod){
			if(($prod->getPercent() > 0) and ($prod->getPercent() < $min)){
				$min = $prod->getPercent();
			}
		}

		//guardo els nums de la mescla
		foreach($prodsmescla as $prod){
			$num = $prod->getPercent() / $min;
			$prod->setNum($num);
		}

	}

	public function setPreumescla($preumescla){
		return $this;
	}

	public function getPreumescla(){
		$preu = 0;
		$prodsmescla = $this->getProdbase();
		foreach($prodsmescla as $prod){
			$preu = $preu + $prod->getPreupercent();
		}

		return $preu;
	}

	/*********** compostos *********************/
	public function setPreucomposicio($preucomposicio){
		return $this;
	}

	public function getPreucomposicio(){
		$preu = 0;
		$prodscompost = $this->getProdcompost();
		foreach($prodscompost as $prod){
			$preu = $preu + $prod->getSubtotal();
		}

		return $preu;
	}

}