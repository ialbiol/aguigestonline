<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Tipotrebs {
	protected $nom;

 protected $id;

 // @var \Agui\GestBundle\Entity\Proveedors
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $factura;

 public function __construct() {
  $this->factura = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }


	public function getId() {
		return $this->id;
	}

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura[] = $factura;
  return $this;
 }

 public function removeFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura->removeElement($factura);
 }

 public function getFactura(){
  return $this->factura;
 }

	public function __toString() {
		return $this->getNom();
	}

}