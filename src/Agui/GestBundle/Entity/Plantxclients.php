<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Plantxclients {
 protected $data;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $client;
 protected $plantilla;

 public function __construct() {

 }

 public function setData($data){
  $this->data = $data;

  return $this;
 }

 public function getData(){
  return $this->data;
 }


	public function getId() {
		return $this->id;
	}

 public function setClient(\Agui\GestBundle\Entity\Clients $client = null) {
  $this->client = $client;
  return $this;
 }

 public function getClient() {
  return $this->client;
 }

 public function setPlantilla(\Agui\GestBundle\Entity\Plantilles $plantilla = null) {
  $this->plantilla = $plantilla;
  return $this;
 }

 public function getPlantilla() {
  return $this->plantilla;
 }

	public function __toString() {
		return $this->getData();
	}

}