<?php
namespace Agui\GestBundle\Entity;

class Auxselects {
 public $empreses;

 public static function setEmpreses($empreses){
  $this->empreses = $empreses;
 }

 public static function getEmpreses(){
  return $this->empreses;
 }

	public static function GetYesNo() {
		return array(
			'0' => 'No',
			'1' => 'Yes',
		);
	}

	public static function GetNaYesNo() {
		return array(
			'-1' => 'N/A',
			'0' => 'No',
			'1' => 'Yes',
		);
	}

	public static function GetFiltroProducte() {
		return array(
			'0' => 'Producte normal',
			'1' => 'Producte albaran',
   '2' => 'Descatalogat',
		);
	}

	public static function FindFiltroProducte($id) {
		$m = Auxselects::GetFiltroProducte();
		return $m[$id];
	}

 public static function GetLlibre(){
  return array(
   '0' => 'compres',
   //'x' => '(variacio dexistencies no)',
   '1' => 'lloguers i canons',
   '2' => ' reparacions i conservacio',
   '3' => 'treballs altres empreses',
   '4' => 'transports',
   '5' => 'assegurances',
   '6' => 'aigua llum gas combustibles',
   '7' => 'tributs no estatals',
   //'8' => 'sous', //(no entra al llibre)
   //'9' => 'seg social i autonoms', //(no entra al llibre)
   '10' => 'despeses financieres',
   '11' => 'serveis',
   '50' => 'hipotca', //(no llibre-ultim)
   '51' => 'sous', //(no entra al llibre)
   '52' => 'seg social i autonoms', //(no entra al llibre)
   '53' => 'comisions.... ',
   '54' => 'iva avançat',
   '55' => 'moduls',
   '56' => 'altres gastos no detallats',
  );
 }

 public static function GetTipoIva(){
  return array(
   '0',
   '1',
   '2',
   '3',
   '4',
   '5',
   '6',
   '7',
   '8',
   '9',
   '10',
   '11',
   '12',
   '13',
   '14',
   '15',
   '16',
   '17',
   '18',
   '19',
   '20',
   '21',
   '22',
   '23',
   '24',
   '25',
   '26',
   '27',
   '28',
   '29',
   '30'
  );
 }

 public static function GetTipoDte(){
  return array(
   '0',
   '1',
   '2',
   '3',
   '4',
   '5',
   '6',
   '7',
   '8',
   '9',
   '10',
   '11',
   '12',
   '13',
   '14',
   '15',
   '16',
   '17',
   '18',
   '19',
   '20',
   '21',
   '22',
   '23',
   '24',
   '25',
   '26',
   '27',
   '28',
   '29',
   '30'
  );
 }

	public static function GetEstatusFactCompra() {
		return array(
			0 => 'Pendent',
			10 => 'Pagada',
		);
	}

	public static function GetTContaFactures() {
		return array(
			'0' => 'F',
			'1' => 'R',
		);
	}

	public static function GetTipoClient() {
		return array(
			'10' => 'Particular',
			'1' => 'Empresa',
		);
	}

	public static function GetHistoricClient() {
		return array(
			'0' => 'En us',
			'1' => 'Historic',
		);
	}

	public static function GetPlantillesModels(){ //idtoca
		return array(
			'0' => 'Normal',
			'1' => 'Model',
			'2' => 'Especial',
			'3' => 'Descatalogada',
		);
	}

	public static function GetPlantillesTipo(){ //tipoun
		return array(
			'0' => 'Plantilla normal',
			'1' => 'Plantilla per unitat',
		);
	}

	public static function GetPlantillesClasesTipo(){ //tipos de tipos de plantilles
		return array(
			'0' => 'Clients',
			'1' => 'Tipo faena',
			'2' => 'Clase faena',
			'3' => 'Aplicació sobre',
			'4' => 'Acabat',
			'5' => 'Clase acabat',
		);
	}

	public static function FindPlantillesModels($id) {
		$m = Auxselects::GetPlantillesModels();
		return $m[$id];
	}

	public static function FindPlantillesClasesTipo($id) {
		$m = Auxselects::GetPlantillesClasesTipo();
		return $m[$id];
	}

	public static function GetEstatsPressupostos(){
		return array(
			'0' => 'En projece',
			'1' => 'Presentat',
			'10' => 'Acceptat',
			'9' => 'Rebutjat',
		);
	}

	public static function GetEstatsTreballs(){
		return array(
			'10' => 'Pendent',
			'11' => 'En proces',
			'12' => 'Pausat',
			'20' => 'Acabat',
		);
	}

	public static function GetEstatsFactures(){
		return array(
			'20' => 'Pendent',
			'21' => 'Revisada',
			'22' => 'Presentada',
   '29' => 'Agrupada',
			'30' => 'Cobrada',
			'90' => 'Rebuig',
		);
	}

	public static function FindEstatsPressupostos($id) {
		$m = Auxselects::GetEstatsPressupostos();
		return $m[$id];
	}

	public static function FindEstatsTreballs($id) {
		$m = Auxselects::GetEstatsTreballs();
		return $m[$id];
	}

	public static function FindEstatsFactures($id) {
		$m = Auxselects::GetEstatsFactures();
		return $m[$id];
	}

	public static function GetTipoPressupost() {
		return array(
			'0' => 'Pressupost obert',
			'1' => 'Pressupost tancat',
		);
	}

	public static function GetTreballadorActiu() {
		return array(
			'0' => 'Inactiu',
			'1' => 'Actiu',
		);
	}

	public static function GetGrupFotos() {
		return array(
			'1' => 'Fotos producte',
			'2' => 'Fotos plantilles',
   '3' => 'fotos treballs',
		);
	}

	public static function FindGrupFotos($id) {
		$m = Auxselects::GetGrupFotos();
		return $m[$id];
	}

}