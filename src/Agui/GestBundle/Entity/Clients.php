<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Clients {
	protected $nom;
	protected $direccio;
 protected $telf;
 protected $nif;
 protected $percent;
 protected $tipoclient;
 protected $notes;
 protected $ncompte;
 protected $malnom;
 protected $email;
 protected $localitat;
 protected $telf2;
 protected $mobil1;
 protected $mobil2;
 protected $info;
 protected $formules;
 protected $telf3;
 protected $telf4;
 protected $mobil3;
 protected $mobil4;
 protected $ntints;
 protected $histo;

 protected $id;

 // @var \Agui\GestBundle\Entity\Proveedors
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $factura;
 private $facturafact;
 private $plantxclient;
 private $tipoplan;

 public function __construct() {
  $this->factura = new \Doctrine\Common\Collections\ArrayCollection();
  $this->facturafact = new \Doctrine\Common\Collections\ArrayCollection();
  $this->plantxclient = new \Doctrine\Common\Collections\ArrayCollection();
  $this->tipoplan = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setDireccio($direccio){
  $this->direccio = $direccio;

  return $this;
 }

 public function getDireccio(){
  return $this->direccio;
 }

 public function setTelf($telf){
  $this->telf = $telf;

  return $this;
 }

 public function getTelf(){
  return $this->telf;
 }

 public function setNif($nif){
  $this->nif = $nif;

  return $this;
 }

 public function getNif(){
  return $this->nif;
 }

 public function setPercent($percent){
  $this->percent = $percent;

  return $this;
 }

 public function getPercent(){
  return $this->percent;
 }

 public function setTipoclient($tipoclient){
  $this->tipoclient = $tipoclient;

  return $this;
 }

 public function getTipoclient(){
  return $this->tipoclient;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setNcompte($ncompte){
  $this->ncompte = $ncompte;

  return $this;
 }

 public function getNcompte(){
  return $this->ncompte;
 }

 public function setMalnom($malnom){
  $this->malnom = $malnom;

  return $this;
 }

 public function getMalnom(){
  return $this->malnom;
 }

 public function setEmail($email){
  $this->email = $email;

  return $this;
 }

 public function getEmail(){
  return $this->email;
 }

 public function setLocalitat($localitat){
  $this->localitat = $localitat;

  return $this;
 }

 public function getLocalitat(){
  return $this->localitat;
 }

 public function setTelf2($telf2){
  $this->telf2 = $telf2;

  return $this;
 }

 public function getTelf2(){
  return $this->telf2;
 }

 public function setMobil1($mobil1){
  $this->mobil1 = $mobil1;

  return $this;
 }

 public function getMobil1(){
  return $this->mobil1;
 }

 public function setMobil2($mobil2){
  $this->mobil2 = $mobil2;

  return $this;
 }

 public function getMobil2(){
  return $this->mobil2;
 }

 public function setInfo($info){
  $this->info = $info;

  return $this;
 }

 public function getInfo(){
  return $this->info;
 }

 public function setFormules($formules){
  $this->formules = $formules;

  return $this;
 }

 public function getFormules(){
  return $this->formules;
 }

 public function setTelf3($telf3){
  $this->telf3 = $telf3;

  return $this;
 }

 public function getTelf3(){
  return $this->telf3;
 }

 public function setTelf4($telf4){
  $this->telf4 = $telf4;

  return $this;
 }

 public function getTelf4(){
  return $this->telf4;
 }

 public function setMobil3($mobil3){
  $this->mobil3 = $mobil3;

  return $this;
 }

 public function getMobil3(){
  return $this->mobil3;
 }

 public function setMobil4($mobil4){
  $this->mobil4 = $mobil4;

  return $this;
 }

 public function getMobil4(){
  return $this->mobil4;
 }

 public function setNtints($ntints){
  $this->ntints = $ntints;

  return $this;
 }

 public function getNtints(){
  return $this->ntints;
 }

 public function setHisto($histo){
  $this->histo = $histo;

  return $this;
 }

 public function getHisto(){
  return $this->histo;
 }


	public function getId() {
		return $this->id;
	}

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura[] = $factura;
  return $this;
 }

 public function removeFactura(\Agui\GestBundle\Entity\Factures $factura){
  $this->factura->removeElement($factura);
 }

 public function getFactura(){
  return $this->factura;
 }

 public function addFacturafact(\Agui\GestBundle\Entity\Factures $facturafact){
  $this->facturafact[] = $facturafact;
  return $this;
 }

 public function removeFacturafact(\Agui\GestBundle\Entity\Factures $facturafact){
  $this->facturafact->removeElement($facturafact);
 }

 public function getFacturafact(){
  return $this->facturafact;
 }

 public function addPlantxclient(\Agui\GestBundle\Entity\Plantxclients $plantxclient){
  $this->plantxclient[] = $plantxclient;
  return $this;
 }

 public function removePlantxclient(\Agui\GestBundle\Entity\Plantxclients $plantxclient){
  $this->plantxclient->removeElement($plantxclient);
 }

 public function getPlantxclient(){
  return $this->plantxclient;
 }

 public function addTipoplan(\Agui\GestBundle\Entity\Tipoplans $tipoplan){
  $this->tipoplan[] = $tipoplan;
  return $this;
 }

 public function removeTipoplan(\Agui\GestBundle\Entity\Tipoplans $tipoplan){
  $this->tipoplan->removeElement($tipoplan);
 }

 public function getTipoplan(){
  return $this->tipoplan;
 }

	public function __toString() {
		return $this->getNom();
	}

}