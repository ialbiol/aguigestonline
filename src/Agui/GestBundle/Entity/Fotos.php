<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Fotos {
	protected $nom;
 protected $ruta;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $tipofoto;
	protected $producte;
	protected $factura;
	protected $treballador;
	protected $plantilla;

	private $file; //aux var to manage the upload information

 public function __construct() {

 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setRuta($ruta){
  $this->ruta = $ruta;

  return $this;
 }

 public function getRuta(){
  return $this->ruta;
 }


	public function getId() {
		return $this->id;
	}

 public function setFactura(\Agui\GestBundle\Entity\Factures $factura = null) {
  $this->factura = $factura;
  return $this;
 }

 public function getFactura() {
  return $this->factura;
 }

 public function setProducte(\Agui\GestBundle\Entity\Productes $producte = null) {
  $this->producte = $producte;
  return $this;
 }

 public function getProducte() {
  return $this->producte;
 }

 public function setTreballador(\Agui\GestBundle\Entity\Treballadors $treballador = null) {
  $this->treballador = $treballador;
  return $this;
 }

 public function getTreballador() {
  return $this->treballador;
 }

 public function setPlantilla(\Agui\GestBundle\Entity\Plantilles $plantilla = null) {
  $this->plantilla = $plantilla;
  return $this;
 }

 public function getPlantilla() {
  return $this->plantilla;
 }

 public function setTipofoto(\Agui\GestBundle\Entity\Tipofotos $tipofoto = null) {
  $this->tipofoto = $tipofoto;
  return $this;
 }

 public function getTipofoto() {
  return $this->tipofoto;
 }



	public function setFile($file){
		$this->file = $file;
		return $this;
	}

	public function getFile(){
		return $this->file;
	}


	public function imageExists(){
		$res = false;
		if($this->nom != ""){
			if(file_exists("../media/".$this->getImagenamewpath())){
				$res = true;
			}
		}
		return $res;
	}


	public function setAttachmentid($attachmentid = null) {
		return $this;
	}

	public function getAttachmentid(){
		return $this->id;
	}

	public function setImagenamewpath($imagenamewpath = null)	{
		return $this;
	}

	public function getImagenamewpath()	{
		$atttype = $this->getTipofoto();
		$path = $this->nom;

		if($atttype == null){ return $path; } //segurity

		$grup = $atttype->getGrup();
		if($grup == 1){ $path = "productes/" . $this->nom; }
		if($grup == 10){ $path = "plantilles/" . $this->nom; }
		if($grup == 20){ $path = "treballs/" . $this->nom; }
		if($grup == 30){ $path = "treballadors/" . $this->nom; }

		return $path;
	}


	public function __toString() {
		return $this->getNom();
	}

}