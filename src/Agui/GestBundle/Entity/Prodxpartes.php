<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Prodxpartes {
 protected $quantitatpre;
 protected $quantitatus;

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $prodxfact;
 protected $parte;

 public function __construct() {

 }

 public function setQuantitatpre($quantitatpre){
  $this->quantitatpre = $quantitatpre;

  return $this;
 }

 public function getQuantitatpre(){
  return $this->quantitatpre;
 }

 public function setQuantitatus($quantitatus){
  $this->quantitatus = $quantitatus;

  return $this;
 }

 public function getQuantitatus(){
  return $this->quantitatus;
 }


	public function getId() {
		return $this->id;
	}

 public function setProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact = null) {
  $this->prodxfact = $prodxfact;
  return $this;
 }

 public function getProdxfact() {
  return $this->prodxfact;
 }

 public function setParte(\Agui\GestBundle\Entity\Partes $parte = null) {
  $this->parte = $parte;
  return $this;
 }

 public function getParte() {
  return $this->parte;
 }

	public function __toString() {
		return $this->getQuantitatpre();
	}

}