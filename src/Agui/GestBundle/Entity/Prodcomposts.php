<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Prodcomposts {
	protected $quantitat;

 protected $id;

 // @var \Agui\GestBundle\Entity\Proveedors
 protected $producte;
 protected $productepart;

 public function __construct() {
 }

 public function setQuantitat($quantitat){
  $this->quantitat = $quantitat;

  return $this;
 }

 public function getQuantitat(){
  return $this->quantitat;
 }

	public function getId() {
		return $this->id;
	}

 public function setProducte(\Agui\GestBundle\Entity\Productes $producte = null) {
  $this->producte = $producte;
  return $this;
 }

 public function getProducte() {
  return $this->producte;
 }

 public function setProductepart(\Agui\GestBundle\Entity\Productes $productepart = null) {
  $this->productepart = $productepart;
  return $this;
 }

 public function getProductepart() {
  return $this->productepart;
 }

	public function __toString() {
		return $this->getNom();
	}

//////////////////////////////////////////////////////////

	public function setPreualbaran($preualbaran){
		return $this;
	}

	public function getPreualbaran(){
		$preu = 0;
		$prod = $this->getProducte();
		if($prod != null){
			$preu = $prod->getPalbaran() / $prod->getUnitats();
		}

		return $preu;
	}

	public function setSubtotal($subtotal){
		return $this;
	}

	public function getSubtotal(){
		$preu = $this->getPreualbaran();
		$preu = $preu * $this->getQuantitat();

		return $preu;
	}

}