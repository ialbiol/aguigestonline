<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Prodxcompres {
 protected $idllibre;
 protected $quantitat = 1;
 protected $cost;
 protected $dte = 0;
 protected $nom = " ";

 protected $id;

 // @var \Agui\GestBundle\Entity\Clients
 protected $producte;
 protected $compra;
 protected $albaran;
 protected $compnum;
 protected $tipoprod;

 public function __construct() {

 }

 public function setIdllibre($idllibre){
  $this->idllibre = $idllibre;

  return $this;
 }

 public function getIdllibre(){
  return $this->idllibre;
 }

 public function setQuantitat($quantitat){
  $this->quantitat = $quantitat;

  return $this;
 }

 public function getQuantitat(){
  return $this->quantitat;
 }

 public function setCost($cost){
  $this->cost = $cost;

  return $this;
 }

 public function getCost(){
  return $this->cost;
 }

 public function setDte($dte){
  $this->dte = $dte;

  return $this;
 }

 public function getDte(){
  return $this->dte;
 }

 public function setNom($nom){
  $this->nom = $nom;
		if($nom == null){
			$this->nom = " ";
		}

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }


	public function getId() {
		return $this->id;
	}

 public function setProducte(\Agui\GestBundle\Entity\Productes $producte = null) {
  $this->producte = $producte;

		$this->setIdllibre($this->producte->getIdllibre());
		$this->setTipoprod($this->producte->getTipoprod());

  return $this;
 }

 public function getProducte() {
  return $this->producte;
 }

 public function setCompra(\Agui\GestBundle\Entity\Compres $compra = null) {
  $this->compra = $compra;
  return $this;
 }

 public function getCompra() {
  return $this->compra;
 }

 public function setAlbaran(\Agui\GestBundle\Entity\Albarans $albaran = null) {
  $this->albaran = $albaran;
  return $this;
 }

 public function getAlbaran() {
  return $this->albaran;
 }

 public function setCompnum(\Agui\GestBundle\Entity\Compnums $compnum = null) {
  $this->compnum = $compnum;
  return $this;
 }

 public function getCompnum() {
  return $this->compnum;
 }

 public function setTipoprod(\Agui\GestBundle\Entity\Tipoprods $tipoprod = null) {
  $this->tipoprod = $tipoprod;
  return $this;
 }

 public function getTipoprod() {
  return $this->tipoprod;
 }

	public function __toString() {
		return $this->getNom();
	}

///////////////////////////////////////////

 public function setTotal($total){
  return $this;
 }

 public function getTotal(){
  $base = $this->quantitat * $this->cost;
  $dte = $base * ($this->dte / 100);
		$total = $base - $dte;
  return $total;
 }

}