<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Telfs {
	protected $nom;
	protected $direccio;
 protected $telf1;
 protected $notes;
 protected $malnom;
 protected $telf2;
 protected $mobil1;
 protected $mobil2;
 protected $telf3;
 protected $telf4;
 protected $mobil3;
 protected $mobil4;
	protected $email;

 protected $id;
 
 public function __construct() {

 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setDireccio($direccio){
  $this->direccio = $direccio;

  return $this;
 }

 public function getDireccio(){
  return $this->direccio;
 }

 public function setTelf1($telf1){
  $this->telf1 = $telf1;

  return $this;
 }

 public function getTelf1(){
  return $this->telf1;
 }

 public function setNotes($notes){
  $this->notes = $notes;

  return $this;
 }

 public function getNotes(){
  return $this->notes;
 }

 public function setMalnom($malnom){
  $this->malnom = $malnom;

  return $this;
 }

 public function getMalnom(){
  return $this->malnom;
 }

 public function setTelf2($telf2){
  $this->telf2 = $telf2;

  return $this;
 }

 public function getTelf2(){
  return $this->telf2;
 }

 public function setMobil1($mobil1){
  $this->mobil1 = $mobil1;

  return $this;
 }

 public function getMobil1(){
  return $this->mobil1;
 }

 public function setMobil2($mobil2){
  $this->mobil2 = $mobil2;

  return $this;
 }

 public function getMobil2(){
  return $this->mobil2;
 }

 public function setTelf3($telf3){
  $this->telf3 = $telf3;

  return $this;
 }

 public function getTelf3(){
  return $this->telf3;
 }

 public function setTelf4($telf4){
  $this->telf4 = $telf4;

  return $this;
 }

 public function getTelf4(){
  return $this->telf4;
 }

 public function setMobil3($mobil3){
  $this->mobil3 = $mobil3;

  return $this;
 }

 public function getMobil3(){
  return $this->mobil3;
 }

 public function setMobil4($mobil4){
  $this->mobil4 = $mobil4;

  return $this;
 }

 public function getMobil4(){
  return $this->mobil4;
 }

 public function setEmail($email){
  $this->email = $email;

  return $this;
 }

 public function getEmail(){
  return $this->email;
 }


	public function getId() {
		return $this->id;
	}

	public function __toString() {
		return $this->getNom();
	}

}