<?php
namespace Agui\GestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Tipoprods {
	protected $nom;
	protected $idfiltro;

 protected $id;

 // @var \Agui\GestBundle\Entity\Proveedors
 protected $empresa;

 // @var \Doctrine\Common\Collections\Collection
 private $producte;
 private $prodxcompra;
 private $prodxfact;
 private $prodxplant;

 public function __construct() {
  $this->producte = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxcompra = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxfact = new \Doctrine\Common\Collections\ArrayCollection();
  $this->prodxplant = new \Doctrine\Common\Collections\ArrayCollection();
 }

 public function setNom($nom){
  $this->nom = $nom;

  return $this;
 }

 public function getNom(){
  return $this->nom;
 }

 public function setIdfiltro($idfiltro){
  $this->idfiltro = $idfiltro;

  return $this;
 }

 public function getIdfiltro(){
  return $this->idfiltro;
 }


	public function getId() {
		return $this->id;
	}

 public function setEmpresa(\Agui\GestBundle\Entity\Empreses $empresa = null) {
  $this->empresa = $empresa;
  return $this;
 }

 public function getEmpresa() {
  return $this->empresa;
 }

 public function addProducte(\Agui\GestBundle\Entity\Productes $producte){
  $this->producte[] = $producte;
  return $this;
 }

 public function removeProducte(\Agui\GestBundle\Entity\Productes $producte){
  $this->producte->removeElement($producte);
 }

 public function getProducte(){
  return $this->producte;
 }

 public function addProdxcompra(\Agui\GestBundle\Entity\Prodxcompres $prodxcompra){
  $this->prodxcompra[] = $prodxcompra;
  return $this;
 }

 public function removeProdxcompra(\Agui\GestBundle\Entity\Prodxcompres $prodxcompra){
  $this->prodxcompra->removeElement($prodxcompra);
 }

 public function getProdxcompra(){
  return $this->prodxcompra;
 }

 public function addProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact){
  $this->prodxfact[] = $prodxfact;
  return $this;
 }

 public function removeProdxfact(\Agui\GestBundle\Entity\Prodxfacts $prodxfact){
  $this->prodxfact->removeElement($prodxfact);
 }

 public function getProdxfact(){
  return $this->prodxfact;
 }

 public function addProdxplant(\Agui\GestBundle\Entity\Prodxplants $prodxplant){
  $this->prodxplant[] = $prodxplant;
  return $this;
 }

 public function removeProdxplant(\Agui\GestBundle\Entity\Prodxplants $prodxplant){
  $this->prodxplant->removeElement($prodxplant);
 }

 public function getProdxplant(){
  return $this->prodxplant;
 }

	public function __toString() {
		return $this->getNom();
	}

}