USE [NBMR]
GO

/****** Object:  Table [dbo].[fos_user_tasks]    Script Date: 09/02/2016 8:33:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[fos_user_tasks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[iduser] [int] NULL,
	[task] [nvarchar](max) NULL,
	[begindate] [datetime2](6) NULL,
	[enddate] [datetime2](6) NULL,
 CONSTRAINT [PK_fos_user_tasks_Id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[fos_user_tasks] ADD  CONSTRAINT [DF__fos_user___IdUse__3A4CA8FD]  DEFAULT (NULL) FOR [iduser]
GO

ALTER TABLE [dbo].[fos_user_tasks] ADD  CONSTRAINT [DF__fos_user___Begin__3B40CD36]  DEFAULT (NULL) FOR [begindate]
GO

ALTER TABLE [dbo].[fos_user_tasks] ADD  CONSTRAINT [DF__fos_user___EndDa__3C34F16F]  DEFAULT (NULL) FOR [enddate]
GO

ALTER TABLE [dbo].[fos_user_tasks]  WITH NOCHECK ADD  CONSTRAINT [fos_user_tasks$fk_fos_user_tasks_fos_user_user1] FOREIGN KEY([iduser])
REFERENCES [dbo].[fos_user_user] ([id])
GO

ALTER TABLE [dbo].[fos_user_tasks] CHECK CONSTRAINT [fos_user_tasks$fk_fos_user_tasks_fos_user_user1]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'NBMR.fos_user_tasks' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fos_user_tasks'
GO


