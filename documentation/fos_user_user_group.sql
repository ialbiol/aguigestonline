USE [NBMR]
GO

/****** Object:  Table [dbo].[fos_user_user_group]    Script Date: 09/02/2016 8:34:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[fos_user_user_group](
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
 CONSTRAINT [PK_fos_user_user_group_user_id] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[fos_user_user_group]  WITH NOCHECK ADD  CONSTRAINT [fos_user_user_group$FK_B3C77447A76ED395] FOREIGN KEY([user_id])
REFERENCES [dbo].[fos_user_user] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[fos_user_user_group] CHECK CONSTRAINT [fos_user_user_group$FK_B3C77447A76ED395]
GO

ALTER TABLE [dbo].[fos_user_user_group]  WITH NOCHECK ADD  CONSTRAINT [fos_user_user_group$FK_B3C77447FE54D947] FOREIGN KEY([group_id])
REFERENCES [dbo].[fos_user_group] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[fos_user_user_group] CHECK CONSTRAINT [fos_user_user_group$FK_B3C77447FE54D947]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'NBMR.fos_user_user_group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fos_user_user_group'
GO


