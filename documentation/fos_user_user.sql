USE [NBMR]
GO

/****** Object:  Table [dbo].[fos_user_user]    Script Date: 09/02/2016 8:34:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[fos_user_user](
	[id] [int] IDENTITY(14,1) NOT NULL,
	[username] [nvarchar](255) NOT NULL,
	[username_canonical] [nvarchar](255) NOT NULL,
	[email] [nvarchar](255) NOT NULL,
	[email_canonical] [nvarchar](255) NOT NULL,
	[enabled] [smallint] NOT NULL,
	[salt] [nvarchar](255) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[last_login] [datetime2](6) NULL CONSTRAINT [DF__fos_user___last___3E1D39E1]  DEFAULT (NULL),
	[locked] [smallint] NOT NULL,
	[expired] [smallint] NOT NULL,
	[expires_at] [datetime] NULL CONSTRAINT [DF__fos_user___expir__3F115E1A]  DEFAULT (NULL),
	[confirmation_token] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___confi__40058253]  DEFAULT (NULL),
	[password_requested_at] [datetime2](6) NULL CONSTRAINT [DF__fos_user___passw__40F9A68C]  DEFAULT (NULL),
	[roles] [nvarchar](max) NOT NULL,
	[credentials_expired] [smallint] NOT NULL,
	[credentials_expire_at] [datetime2](6) NULL CONSTRAINT [DF__fos_user___crede__41EDCAC5]  DEFAULT (NULL),
	[created_at] [datetime2](6) NOT NULL,
	[updated_at] [datetime2](6) NOT NULL,
	[date_of_birth] [datetime2](6) NULL CONSTRAINT [DF__fos_user___date___42E1EEFE]  DEFAULT (NULL),
	[firstname] [nvarchar](64) NULL CONSTRAINT [DF__fos_user___first__43D61337]  DEFAULT (NULL),
	[lastname] [nvarchar](64) NULL CONSTRAINT [DF__fos_user___lastn__44CA3770]  DEFAULT (NULL),
	[website] [nvarchar](64) NULL CONSTRAINT [DF__fos_user___websi__45BE5BA9]  DEFAULT (NULL),
	[biography] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___biogr__46B27FE2]  DEFAULT (NULL),
	[gender] [nvarchar](1) NULL CONSTRAINT [DF__fos_user___gende__47A6A41B]  DEFAULT (NULL),
	[locale] [nvarchar](8) NULL CONSTRAINT [DF__fos_user___local__489AC854]  DEFAULT (NULL),
	[timezone] [nvarchar](64) NULL CONSTRAINT [DF__fos_user___timez__498EEC8D]  DEFAULT (NULL),
	[phone] [nvarchar](64) NULL CONSTRAINT [DF__fos_user___phone__4A8310C6]  DEFAULT (NULL),
	[facebook_uid] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___faceb__4B7734FF]  DEFAULT (NULL),
	[facebook_name] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___faceb__4C6B5938]  DEFAULT (NULL),
	[facebook_data] [nvarchar](max) NULL,
	[twitter_uid] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___twitt__4D5F7D71]  DEFAULT (NULL),
	[twitter_name] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___twitt__4E53A1AA]  DEFAULT (NULL),
	[twitter_data] [nvarchar](max) NULL,
	[gplus_uid] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___gplus__4F47C5E3]  DEFAULT (NULL),
	[gplus_name] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___gplus__503BEA1C]  DEFAULT (NULL),
	[gplus_data] [nvarchar](max) NULL,
	[token] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___token__51300E55]  DEFAULT (NULL),
	[two_step_code] [nvarchar](255) NULL CONSTRAINT [DF__fos_user___two_s__5224328E]  DEFAULT (NULL),
 CONSTRAINT [PK_fos_user_user_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [fos_user_user$UNIQ_C560D76192FC23A8] UNIQUE NONCLUSTERED 
(
	[username_canonical] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [fos_user_user$UNIQ_C560D761A0D96FBF] UNIQUE NONCLUSTERED 
(
	[email_canonical] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'NBMR.fos_user_user' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fos_user_user'
GO


