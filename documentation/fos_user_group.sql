USE [NBMR]
GO

/****** Object:  Table [dbo].[fos_user_group]    Script Date: 09/02/2016 8:30:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[fos_user_group](
	[id] [int] IDENTITY(7,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[roles] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_fos_user_group_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [fos_user_group$UNIQ_583D1F3E5E237E06] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'NBMR.fos_user_group' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'fos_user_group'
GO