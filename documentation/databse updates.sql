-- creo taules fos user
-- user admin / minda
CREATE TABLE `fos_user_group` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(255) NULL DEFAULT NULL , `roles` TEXT NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

insert into fos_user_group (name, role) values ('GROUP_SUPER_ADMIN', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}');


CREATE TABLE `fos_user_user` ( `id` INT NOT NULL AUTO_INCREMENT , `username` VARCHAR(255) NULL DEFAULT NULL , `username_canonical` VARCHAR(255) NULL DEFAULT NULL , `email` VARCHAR(255) NULL DEFAULT NULL , `email_canonical` VARCHAR(255) NULL DEFAULT NULL , `enabled` SMALLINT NULL DEFAULT NULL , `salt` VARCHAR(255) NULL DEFAULT NULL , `password` VARCHAR(255) NULL DEFAULT NULL , `last_login` VARCHAR(255) NULL DEFAULT NULL , `locked` SMALLINT NULL DEFAULT NULL , `expired` SMALLINT NULL DEFAULT NULL , `expires_at` DATETIME NULL DEFAULT NULL , `confirmation_token` VARCHAR(255) NULL DEFAULT NULL , `password_requested_at` DATETIME NULL DEFAULT NULL , `roles` TEXT NULL DEFAULT NULL , `credentials_expired` SMALLINT NULL DEFAULT NULL , `credentials_expire_at` DATETIME NULL DEFAULT NULL , `created_at` DATETIME NULL DEFAULT NULL , `updated_at` DATETIME NULL DEFAULT NULL , `date_of_birth` DATETIME NULL DEFAULT NULL , `firstname` VARCHAR(64) NULL DEFAULT NULL , `lastname` VARCHAR(64) NULL DEFAULT NULL , `website` VARCHAR(64) NULL DEFAULT NULL , `biography` VARCHAR(255) NULL DEFAULT NULL , `gender` VARCHAR(1) NULL DEFAULT NULL , `locale` VARCHAR(8) NULL DEFAULT NULL , `timezone` VARCHAR(64) NULL DEFAULT NULL , `phone` VARCHAR(64) NULL DEFAULT NULL , `facebook_uid` VARCHAR(255) NULL DEFAULT NULL , `facebook_name` VARCHAR(255) NULL DEFAULT NULL , `facebook_data` TEXT NULL DEFAULT NULL , `twitter_uid` VARCHAR(255) NULL DEFAULT NULL , `twitter_name` VARCHAR(255) NULL DEFAULT NULL , `twitter_data` TEXT NULL DEFAULT NULL , `gplus_uid` VARCHAR(255) NULL DEFAULT NULL , `gplus_name` VARCHAR(255) NULL DEFAULT NULL , `gplus_data` TEXT NULL DEFAULT NULL , `token` VARCHAR(255) NULL DEFAULT NULL , `two_step_code` VARCHAR(255) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO `fos_user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES (NULL, 'admin', 'admin', 'admin@admin.com', 'admin@admin.com', '1', '24h8kwr7k6u8osk8wocgos40okc80sc', 'UWweXnQUmkPgWKMp9LuIkCXchciPkSarfcB2lDFkxpXtDJYaiU3soC/oQr8Oty/JV6KfakzDzmEXG2eSmOllQw==', '2015-07-06 12:49:54.000000', '0', '0', '2020-07-06 12:49:54.000000', '', '', 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', '0', '2020-07-06 12:49:54.000000', '2013-05-15 09:20:46.000000', '2015-07-06 12:51:45.000000', '', '', '', '', '', 'u', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

ALTER TABLE `fos_user_user` ADD `mypass` VARCHAR(50) NULL DEFAULT NULL AFTER `two_step_code`;
UPDATE `fos_user_user` SET `mypass` = '#23u#ZYb#s0$' WHERE `fos_user_user`.`id` = 1 


CREATE TABLE `fos_user_user_group` ( `user_id` INT NOT NULL , `group_id` INT NOT NULL ) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

insert into fos_user_user_group (user_id, group_id) values ('1', '1');


CREATE TABLE `fos_user_tasks` ( `id` INT NOT NULL AUTO_INCREMENT , `iduser` INT NOT NULL , `task` TEXT NULL DEFAULT NULL , `begindate` DATETIME NULL DEFAULT NULL , `enddate` DATETIME NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;



-- canvis taula albarans
-- creo camps dates noves
ALTER TABLE `albarans` ADD `DDATA` DATETIME NULL DEFAULT NULL AFTER `DATA`, ADD `DDATAPAGADA` DATETIME NULL DEFAULT NULL AFTER `DATAPAGADA`;
-- poso els valors de les dates correctes
update albarans set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATA AS INTEGER) DAY), DDATAPAGADA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAPAGADA AS INTEGER) DAY);
update albarans set DDATA = NULL where DDATA = '1899-12-12 00:00:00';
update albarans set DDATAPAGADA = NULL where DDATAPAGADA = '1899-12-12 00:00:00';


-- canvis taula compnum
-- creo camps dates noves
ALTER TABLE `compnum` ADD `DDATA` DATETIME NULL DEFAULT NULL AFTER `DATA`, ADD `DDATAPAGADA` DATETIME NULL DEFAULT NULL AFTER `DATAPAGADA`;
-- poso els valors de les dates correctes
update compnum set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATA AS INTEGER) DAY), DDATAPAGADA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAPAGADA AS INTEGER) DAY);
update compnum set DDATA = NULL where DDATA = '1899-12-12 00:00:00';
update compnum set DDATAPAGADA = NULL where DDATAPAGADA = '1899-12-12 00:00:00';


-- canvis taula compres
-- creo camps dates noves
ALTER TABLE `compres` ADD `DDATA` DATETIME NULL DEFAULT NULL AFTER `DATA`, ADD `DDATAPAGADA` DATETIME NULL DEFAULT NULL AFTER `DATAPAGADA`;
-- poso els valors de les dates correctes
update compres set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATA AS INTEGER) DAY), DDATAPAGADA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAPAGADA AS INTEGER) DAY);
update compres set DDATA = NULL where DDATA = '1899-12-12 00:00:00';
update compres set DDATAPAGADA = NULL where DDATAPAGADA = '1899-12-12 00:00:00';


-- canvis taula factures
-- creo camps dates noves
ALTER TABLE `factures` ADD `DDATAPRES` DATETIME NULL DEFAULT NULL AFTER `DATAPRES`, ADD `DDATAFACT` DATETIME NULL DEFAULT NULL AFTER `DATAFACT`, ADD `DDATAPRESENTAT` DATETIME NULL DEFAULT NULL AFTER `DATAPRESENTAT`, ADD `DDATAACEPTAT` DATETIME NULL DEFAULT NULL AFTER `DATAACEPTAT`, ADD `DDATAENTREGA` DATETIME NULL DEFAULT NULL AFTER `DATAENTREGA`, ADD `DDATAENPROCES` DATETIME NULL DEFAULT NULL AFTER `DATAENPROCES`, ADD `DDATAACABAT` DATETIME NULL DEFAULT NULL AFTER `DATAACABAT`, ADD `DDATAFACTPRESENTADA` DATETIME NULL DEFAULT NULL AFTER `DATAFACTPRESENTADA`, ADD `DDATACOBRO` DATETIME NULL DEFAULT NULL AFTER `DATACOBRO`;
-- poso els valors de les dates correctes
update factures set DDATAPRES = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAPRES AS INTEGER) DAY)
, DDATAFACT = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAFACT AS INTEGER) DAY)
, DDATAPRESENTAT = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAPRESENTAT AS INTEGER) DAY)
, DDATAACEPTAT = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAACEPTAT AS INTEGER) DAY)
, DDATAENTREGA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAENTREGA AS INTEGER) DAY)
, DDATAENPROCES = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAENPROCES AS INTEGER) DAY)
, DDATAACABAT = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAACABAT AS INTEGER) DAY)
, DDATAFACTPRESENTADA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATAFACTPRESENTADA AS INTEGER) DAY)
, DDATACOBRO = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATACOBRO AS INTEGER) DAY)
;

update factures set DDATAPRES = NULL where DDATAPRES = '1899-12-12 00:00:00';
update factures set DDATAFACT = NULL where DDATAFACT = '1899-12-12 00:00:00';
update factures set DDATAPRESENTAT = NULL where DDATAPRESENTAT = '1899-12-12 00:00:00';
update factures set DDATAACEPTAT = NULL where DDATAACEPTAT = '1899-12-12 00:00:00';
update factures set DDATAENTREGA = NULL where DDATAENTREGA = '1899-12-12 00:00:00';
update factures set DDATAENPROCES = NULL where DDATAENPROCES = '1899-12-12 00:00:00';
update factures set DDATAACABAT = NULL where DDATAACABAT = '1899-12-12 00:00:00';
update factures set DDATAFACTPRESENTADA = NULL where DDATAFACTPRESENTADA = '1899-12-12 00:00:00';
update factures set DDATACOBRO = NULL where DDATACOBRO = '1899-12-12 00:00:00';



-- canvis taula pagaments
-- creo camps dates noves
ALTER TABLE `pagaments` ADD `DDATA` DATETIME NULL DEFAULT NULL AFTER `DATA`;
-- poso els valors de les dates correctes
update pagaments set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATA AS INTEGER) DAY);
update pagaments set DDATA = NULL where DDATA = '1899-12-12 00:00:00';



-- canvis taula partes
-- creo camps dates noves
ALTER TABLE `partes` ADD `DDATA` DATETIME NULL DEFAULT NULL AFTER `DATA`, ADD `DHINICI` DATETIME NULL DEFAULT NULL AFTER `HINICI`, ADD `DHFINAL` DATETIME NULL DEFAULT NULL AFTER `HFINAL`;
-- poso els valors de les dates correctes
update partes set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATA AS INTEGER) DAY);
update partes set DDATA = NULL where DDATA = '1899-12-12 00:00:00';



-- canvis taula plantxclient
-- creo camps dates noves
ALTER TABLE `planing` ADD `DData` DATETIME NULL DEFAULT NULL AFTER `ESTAT`;
-- poso els valors de les dates correctes
update planing set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DIA AS INTEGER) DAY);
update planing set DDATA = NULL where DDATA = '1899-12-12 00:00:00';



-- canvis taula plantxclient
-- creo camps dates noves
ALTER TABLE `plantxclient` ADD `DDATA` DATETIME NULL DEFAULT NULL AFTER `DATA`;
-- poso els valors de les dates correctes
update plantxclient set DDATA = DATE_ADD('1899-12-12 00:00:00', INTERVAL CAST(DATA AS INTEGER) DAY);
update plantxclient set DDATA = NULL where DDATA = '1899-12-12 00:00:00';


-- crear taula empreses
CREATE TABLE `aguigest`.`empreses` ( `idempresa` INT NOT NULL AUTO_INCREMENT , `nom` VARCHAR(50) NULL DEFAULT NULL, `idtarifa` INT NOT NULL , PRIMARY KEY (`idempresa`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

-- creo les dades de la taula
-- 0 - totes, 1-lacats aguilera, 2-equipo madera
insert into empreses (nom, idtarifa) values ('Lacats Aguilera', 1);
insert into empreses (nom, idtarifa) values ('Equipo Madera', 2);
insert into empreses (nom, idtarifa) values ('Totes', 1);
-- s'ha de fer un update del valor autonumeric de l'ultim registre per a posar-lo a cero. es fa a manita. COMPTE!!!!!
UPDATE `empreses` SET `idempresa` = '0' WHERE `empreses`.`idempresa` = 3;
-- creo camps gestio multiempresa
ALTER TABLE `albarans` ADD `idempresa` INT NOT NULL AFTER `ACOMPTE`;
ALTER TABLE `clients` ADD `idempresa` INT NOT NULL AFTER `NTints`;
ALTER TABLE `compnum` ADD `idempresa` INT NOT NULL AFTER `ACOMPTE`;
ALTER TABLE `compres` ADD `idempresa` INT NOT NULL AFTER `ACOMPTE`;
ALTER TABLE `factures` ADD `idempresa` INT NOT NULL AFTER `IDClientFact`;
ALTER TABLE `partes` ADD `idempresa` INT NOT NULL AFTER `DESCRIPCIO`;
ALTER TABLE `plantilles` ADD `idempresa` INT NOT NULL AFTER `NOTES2`;
ALTER TABLE `productes` ADD `idempresa` INT NOT NULL AFTER `Marca`;
ALTER TABLE `proveedors` ADD `idempresa` INT NOT NULL AFTER `MOBIL4`;
ALTER TABLE `tipoplan` ADD `idempresa` INT NOT NULL AFTER `IDClient`;
ALTER TABLE `tipoprod` ADD `idempresa` INT NOT NULL AFTER `IDFILTRO`;
ALTER TABLE `tipotreb` ADD `idempresa` INT NOT NULL AFTER `NOM`;
ALTER TABLE `treballadors` ADD `idempresa` INT NOT NULL AFTER `NOTES`;
-- inserto valors actuals. tot es de lacats aguilera
update `albarans` set idempresa=1;
update `clients` set idempresa=1;
update `compnum` set idempresa=1;
update `compres` set idempresa=1;
update `factures` set idempresa=1;
update `partes` set idempresa=1;
update `plantilles` set idempresa=1;
update `productes` set idempresa=1;
update `proveedors` set idempresa=1;
update `tipoplan` set idempresa=1;
update `tipoprod` set idempresa=1;
update `tipotreb` set idempresa=1;
update `treballadors` set idempresa=1;

--empreses x usuaris
CREATE TABLE `aguigest`.`empresaxusers` ( `id` INT NOT NULL AUTO_INCREMENT , `idempresa` INT NOT NULL , `iduser` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO `empresaxusers` (`idempresa`, `iduser`) VALUES ('0', '1'), ('1', '1'), ('2', '1');


-- creo productes compostos
CREATE TABLE `aguigest`.`prodcompost` ( `idcomposicio` INT NOT NULL AUTO_INCREMENT , `idproducte` INT NOT NULL , `idproductepart` INT NOT NULL , `quantitat` FLOAT NULL DEFAULT NULL , PRIMARY KEY (`idcomposicio`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;


-- creo camo email a la taula dels proveedors
ALTER TABLE `proveedors` ADD `EMAIL` VARCHAR(255) NULL DEFAULT NULL AFTER `idempresa`;

-- modifico la taula prodxcompra per a aceptar nulls al camp idalbaran
ALTER TABLE `prodxcompra` CHANGE `IDAlbaran` `IDAlbaran` INT(11) NULL DEFAULT '0';
ALTER TABLE `prodxcompra` CHANGE `IDCompNum` `IDCompNum` INT(11) NULL DEFAULT '0';

-- modificacions taula productes
ALTER TABLE `productes` ADD `foto` TEXT NULL DEFAULT NULL AFTER `idempresa`;


-- modificacions taula fotos
ALTER TABLE `fotos` ADD `idtipofoto` INT NULL DEFAULT NULL AFTER `RUTA`, ADD `idproducte` INT NULL DEFAULT NULL AFTER `idtipofoto`;

ALTER TABLE `fotos` CHANGE `IdFactura` `IdFactura` INT(11) NULL DEFAULT NULL, CHANGE `Nom` `Nom` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `RUTA` `RUTA` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `fotos` ADD `idplantilla` INT NULL DEFAULT NULL AFTER `idproducte`, ADD `idtreballador` INT NULL DEFAULT NULL AFTER `idplantilla`;



-- creo taula tipo fotos
CREATE TABLE `tipofoto` ( `idtipofoto` INT NOT NULL AUTO_INCREMENT , `grup` INT NOT NULL DEFAULT '1' , `nom` TEXT NULL , `orden` TINYINT NULL DEFAULT '1' , PRIMARY KEY (`idtipofoto`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;


-- creo taula tarifes
CREATE TABLE `tarifes`. ( `idtarifa` INT NOT NULL AUTO_INCREMENT , `nom` VARCHAR(60) NOT NULL , `datainici` DOUBLE NULL DEFAULT NULL , `datafin` DOUBLE NULL DEFAULT NULL , `ddatainici` DATETIME NULL DEFAULT NULL , `ddatafin` DATETIME NULL DEFAULT NULL , PRIMARY KEY (`idtarifa`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `tarifes` ADD `idempresa` INT NOT NULL AFTER `ddatafin`;

INSERT INTO `tarifes` (`idtarifa`, `nom`, `datainici`, `datafin`, `ddatainici`, `ddatafin`, `idempresa`) VALUES (NULL, 'LA tarifa 2015', 0, 0, NULL, NULL, 1)
INSERT INTO `tarifes` (`idtarifa`, `nom`, `datainici`, `datafin`, `ddatainici`, `ddatafin`, `idempresa`) VALUES (NULL, 'EM tarifa 2016', 0, 0, NULL, NULL, 2)

-- creo taula preus
CREATE TABLE `preus` ( `idpreu` INT NOT NULL AUTO_INCREMENT , `idproducte` INT NOT NULL , `idtarifa` INT NOT NULL , `preu` DOUBLE NOT NULL DEFAULT '0' , PRIMARY KEY (`idpreu`)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

insert into preus (idtarifa, idproducte, preu)
select '1', idproducte, pfactura from productes

ALTER TABLE `factures` ADD `idtarifa` INT NULL AFTER `idempresa`;

UPDATE `factures` SET idtarifa=1

INSERT INTO `tipoprod` (`NOM`, `IDFILTRO`, `idempresa`) VALUES ('temps', '0', '2')


























