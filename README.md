AguiGest Online
=================

Este proyecto quiere gestionar una empresa concreta de lacados, cubriendo todas sus necesidades y particularidades.

Es un proyecto realizado con symphony 2.8, sonata, doctrine y la idea es realizar un front con angular2 y usar redis para acelerar las queries.

Actualmente el proyecto esta en desarrollo y solo incluye el 70% del backoffice y no es usable.