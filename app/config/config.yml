imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: services.yml }

# Put parameters here that don't need to change on each machine where the app is deployed
# http://symfony.com/doc/current/best_practices/configuration.html#application-related-configuration

framework:
    #esi:             ~
    translator:      { fallbacks: ["%locale%"] }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enable_annotations: true }
    #serializer:      { enable_annotations: true }
    templating:
        engines: ['twig']
        #assets_version: SomeVersionScheme
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        # handler_id set to null will use default session handler from php.ini
        handler_id:  ~
    fragments:       ~
    http_method_override: true

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"
    form:
        resources:
            - 'AguiGestBundle:Form:downloadattachment.html.twig'
            - 'AguiGestBundle:Form:image.html.twig'
            - 'AguiGestBundle:Form:icon.html.twig'
    form_themes:
        - 'form/image.html.twig'

# Doctrine Configuration
doctrine:
    dbal:
        default_connection: default
        types:
            json: Sonata\Doctrine\Types\JsonType
        connections:
            default:
                driver:   %database_driver%
                host:     %database_host%
                port:     %database_port%
                dbname:   %database_name%
                user:     %database_user%
                password: %database_password%
                charset:  UTF8

            web:
                driver:   %database_driver_web%
                host:     %database_host_web%
                port:     %database_port_web%
                dbname:   %database_name_web%
                user:     %database_user_web%
                password: %database_password_web%
                charset:  UTF8

    orm:
        default_entity_manager: default
        entity_managers:
            default:
                connection: default
                mappings:
                    AguiGestBundle:  ~
                    FOSUserBundle: ~
                    SonataUserBundle: ~
                    ApplicationSonataUserBundle: ~
                    
                #metadata_cache_driver: xcache
                #query_cache_driver:    xcache
                #result_cache_driver:   xcache

            web:
                connection: web
                mappings:
                    clients:
                        type:      xml
                        dir:       %kernel.root_dir%/../src/Agui/GestBundle/Resources/config/doctrineweb
                        prefix:    Agui\GestBundle\Entityweb
                        alias:     web
                        is_bundle: false


        auto_generate_proxy_classes: "%kernel.debug%"
        #naming_strategy: doctrine.orm.naming_strategy.underscore
        #auto_mapping: true

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    host:      "%mailer_host%"
    username:  "%mailer_user%"
    password:  "%mailer_password%"
    spool:     { type: memory }

assetic:
    debug:          '%kernel.debug%'
    use_controller: false
    filters:
        cssrewrite: ~

knp_paginator:
    page_range: 5                      # default page range used in pagination control
    default_options:
        page_name: page                # page query parameter name
        sort_field_name: sort          # sort field query parameter name
        sort_direction_name: direction # sort direction query parameter name
        distinct: false                 # ensure distinct results, useful when ORM queries are using GROUP BY statements
    template:
       #pagination: KnpPaginatorBundle:Pagination:twitter_bootstrap_v3_pagination.html.twig
        pagination: KnpPaginatorBundle:Pagination:sliding.html.twig     # sliding pagination controls template
        sortable: KnpPaginatorBundle:Pagination:sortable_link.html.twig # sort link template

fos_user:
    db_driver: orm
    firewall_name: main
    user_class: Application\Sonata\UserBundle\Entity\User

    group:
        group_class:   Application\Sonata\UserBundle\Entity\Group
        group_manager: sonata.user.orm.group_manager

    service:
        user_manager: sonata.user.orm.user_manager

#Sonata core
sonata_user:
    admin:                  # Admin Classes
        user:
            class:          Application\Sonata\UserBundle\Admin\UserAdmin

    security_acl: true
    manager_type: orm

sonata_block:
    default_contexts: [cms]
    blocks:
        # enable the SonataAdminBundle block
        sonata.admin.block.admin_list:
            contexts: [admin]
        sonata.admin.block.search_result:
            contexts: [admin]
        #sonata.user.block.menu:    # used to display the menu in profile pages
        #sonata.user.block.account: # used to display menu option (login option)

sonata_admin:
    security:
        handler: sonata.admin.security.handler.role
        # role security information
        information:
            EDIT: EDIT
            LIST: LIST
            CREATE: CREATE
            VIEW: VIEW
            DELETE: DELETE
            EXPORT: EXPORT
            OPERATOR: OPERATOR
            MASTER: MASTER  
            UPDATE: UPDATE

    title:      "AguiGest 2.0"
    title_logo: %title_logo%
    templates:
        # default global templates
        layout:  AguiGestBundle::standard_layout.html.twig
        ajax:    SonataAdminBundle::ajax_layout.html.twig

        # default actions templates, should extend a global templates
        list:    SonataAdminBundle:CRUD:list.html.twig
        show:    SonataAdminBundle:CRUD:show.html.twig
        edit:    SonataAdminBundle:CRUD:edit.html.twig

    # set to true to persist filter settings per admin module in the user's session
    persist_filters: true

sonata_intl:
    timezone:
        # default timezone used as fallback
        default: Europe/Paris

        # locale specific overrides
        locales:
            ca: Europe/Paris
        #    en_UK: Europe/London

sonata_doctrine_orm_admin:
    # default value is null, so doctrine uses the value defined in the configuration
    entity_manager: ~

    templates:
        filter:
            - SonataDoctrineORMAdminBundle:Form:filter_admin_fields.html.twig
        types:
            list:
                image:      AguiGestBundle:CRUD:list_image.html.twig

ps_pdf:
    nodes_file: ~
    fonts_file: ~
    complex_attributes_file: ~
    colors_file: ~
    use_cache_in_stylesheet: ~
    cache:
      type: ~
      options: ~
    markdown_stylesheet_filepath: ~
    markdown_document_template_filepath: ~
    document_parser_type: ~

liip_imagine:
    resolvers:
        default:
            web_path:
                web_root:     %kernel.root_dir%/../web

    loaders:
        default:
            filesystem:
                data_root:  %path_images%

    driver:               gd
    cache:                default
    data_loader:          default

    filter_sets:
        low_thumb:
            quality: 100
            filters:
                 relative_resize: { heighten: 100 }
        mid_thumb:
            quality: 100
            filters:
                 relative_resize: { heighten: 400 }
        high_thumb:
            quality: 100
            filters:
                 relative_resize: { heighten: 900 }
